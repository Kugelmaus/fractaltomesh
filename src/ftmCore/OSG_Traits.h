//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OSG/Array>


// OSG mesh element type definitions
class OSG_Traits : public OpenMesh::DefaultTraits
{
public:
	typedef osg::Vec3 Point;
	typedef osg::Vec3 Normal;
	typedef osg::Vec4 Color;
	typedef osg::Vec2 TexCoord2D;

	//PL: Added 08022015
	//FaceAttributes(OpenMesh::Attributes::Normal);
	VertexAttributes(OpenMesh::Attributes::Status);
	FaceAttributes(OpenMesh::Attributes::Status);
	EdgeAttributes(OpenMesh::Attributes::Status);
	//PL: Added 08022015 Do we need this? In OpenMesh Tutorials not mentioned
	//HalfedgeAttributes(OpenMesh::Attributes::Status);
};

namespace OpenMesh {

	__declspec(dllexport) osg::Vec3 cross(const osg::Vec3& v1, const osg::Vec3& v2);
	__declspec(dllexport) inline float dot(const osg::Vec3& v1, const osg::Vec3& v2);

	__declspec(dllexport) osg::Vec3 om2osg(const Vec3f& v);
	__declspec(dllexport) Vec3f osg2om(const osg::Vec3& v);

}