//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OpenMesh/Core/System/config.hh>
// --------------------
//#include <OpenMesh/Core/Mesh/TriMeshT.hh>
#include <OpenMesh/Core/Mesh/PolyMeshT.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/ArrayKernel.hh>
#include <OpenMesh/Core/Mesh/ArrayItems.hh>
#include <OpenMesh/Core/Mesh/Handles.hh>
#include <OpenMesh/Core/Mesh/FinalMeshItemsT.hh>
#include <OpenMesh/Core/Mesh/TriConnectivity.hh>

#include "BindableAttribKernelT.h"
#include "OSG_PolyMeshT.h"
#include "OSG_Traits.h"

//== NAMESPACES ===============================================================


namespace OpenMesh   {

	//== CLASS DEFINITION =========================================================


	/// Helper class to create a TriMesh-type based on BindableAttribKernelT
	struct OSG_PolyMesh_BindableArrayKernel_Generator
	{
		typedef FinalMeshItemsT<OSG_Traits, true>						MeshItems;
		typedef BindableAttribKernelT<MeshItems, PolyConnectivity>     AttribKernel;
		typedef OSG_PolyMeshT<AttribKernel>							Mesh;
	};



	/** OpenSceneGraph compatible mesh type
	*/
	class OSG_PolyMesh_BindableArrayKernelT
		: public OSG_PolyMesh_BindableArrayKernel_Generator::Mesh
	{};


	//=============================================================================
} // namespace OpenMesh
//=============================================================================