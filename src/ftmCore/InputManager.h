//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################
#pragma once

#include <fstream>
#include <osg/Geode>

namespace ftm{
	class InputManager
	{
		// #### CONSTRUCTOR & DESTRUCTOR ###############
	public:
		InputManager();
		~InputManager(void);


		// #### MEMBER VARIABLES ###############
	private:
		const int* m_edgeTable;
		const int* m_triTable;
		osg::Geode* m_surfaceGeode;
		unsigned int m_volumeDimX;
		unsigned int m_volumeDimY;
		unsigned int m_volumeDimZ;
		float m_zStepSize;
		float m_orthoSize;

		// #### MEMBER FUNCTIONS ###############
	public:
		void loadFEXBIT(int argc, char** argv); //With k�sisch Coding
		static void fex2Param1(int const& in);
		static void fex2Param2(int const& in);
		static void fex2Param3(int const& in);
		static void fexBit_xResolution(int const& in);
		static void fexBit_yResolution(int const& in);
		static void fexBit_zResolution(int const& in);
		static void fexBit_zStepSize(float const& in);
		static void fexBit_orthoSize(float const& in);
		inline unsigned int getVolumeDimX(){ return m_volumeDimX; };
		inline unsigned int getVolumeDimY(){ return m_volumeDimY; };
		inline unsigned int getVolumeDimZ(){ return m_volumeDimZ; };
		inline float getZstepSize(){ return m_zStepSize; };
		inline float getOrthoSize(){ return m_orthoSize; };
		std::vector<bool>* getBoolArray();
	};
}//Namespace