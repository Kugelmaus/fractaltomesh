//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OpenMesh/Core/Utils/vector_traits.hh>
#include <osg/Array>

namespace OpenMesh {

	template<>
	struct vector_traits < osg::Vec2 >
	{
		typedef osg::Vec2Array container_type;

		/// Type of the vector class
		typedef osg::Vec2 vector_type;

		/// Type of the scalar value
		typedef osg::Vec2::value_type value_type;

		/// size/dimension of the vector
		static const size_t size_ = 2;

		/// size/dimension of the vector
		static size_t size() { return size_; }
	};

	template<>
	struct vector_traits < osg::Vec3 >
	{
		typedef osg::Vec3Array container_type;

		/// Type of the vector class
		typedef osg::Vec3 vector_type;

		/// Type of the scalar value
		typedef osg::Vec3::value_type value_type;

		/// size/dimension of the vector
		static const size_t size_ = 3;

		/// size/dimension of the vector
		static size_t size() { return size_; }
	};

	template<>
	struct vector_traits < osg::Vec4 >
	{
		typedef osg::Vec4Array container_type;

		/// Type of the vector class
		typedef osg::Vec4 vector_type;

		/// Type of the scalar value
		typedef osg::Vec4::value_type value_type;

		/// size/dimension of the vector
		static const size_t size_ = 4;

		/// size/dimension of the vector
		static size_t size() { return size_; }
	};

	template<>
	struct vector_traits < GLuint >
	{
		typedef osg::DrawElementsUInt container_type;

		/// Type of the vector class
		typedef GLuint vector_type;

		/// Type of the scalar value
		typedef GLuint value_type;

		/// size/dimension of the vector
		static const size_t size_ = 1;

		/// size/dimension of the vector
		static size_t size() { return size_; }
	};

}