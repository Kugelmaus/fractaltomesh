//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#include "OSG_Traits.h"

namespace OpenMesh {

	__declspec(dllexport) osg::Vec3 cross(const osg::Vec3& v1, const osg::Vec3& v2)
	{
		return v1^v2;
	}


	__declspec(dllexport) float dot(const osg::Vec3& v1, const osg::Vec3& v2)
	{

		return v1 * v2;
	}

	__declspec(dllexport) osg::Vec3 om2osg(const Vec3f& v)
	{
		return osg::Vec3(v[0], v[1], v[2]);
	}

	__declspec(dllexport) Vec3f osg2om(const osg::Vec3& v)
	{
		return Vec3f(v[0], v[1], v[2]);
	}

}