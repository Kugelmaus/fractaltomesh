//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################
#include "BoolMC.h"

//My
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/TrackballManipulator>
#include <osg/ShapeDrawable>
#include <osgGA/StateSetManipulator>

//MyMC
#include <osg/Geometry>
#include <stdlib.h>
#include <osg/ShadeModel>
#include <osg/PolygonMode>
#include <osg/Point>
#include <osg/PositionAttitudeTransform>
#include <osg/ShapeDrawable>
#include <osgDB/WriteFile>
#include <thread>
#include <osgUtil/MeshOptimizers>

//OpenMesh
#include "OpenMeshGeometry.h"

//For calling MeshLab within this exe
#include <shlobj.h>
#include <shlwapi.h>
#include <objbase.h>



// Required to include CUDA vector types
#include <cuda_runtime.h>
#include <cuda_profiler_api.h>
#include <vector_types.h>
//#include <helper_cuda.h>

//Cuda stuff must be compiled with VS2013 compiler, because Cuda have only support for VisualStudio 2015 Update 1, and not Update2!!!
////////////////////////////////////////////////////////////////////////////////
// declaration, forward
extern "C" int testRun(osg::Vec3Array* verticesIn, osg::Vec3Array* verticesTemp, osg::DrawElementsUInt* indiciesIn);

using namespace ftm;


BoolMC::BoolMC(int argc, char** argv) : m_argc(argc), m_argv(argv)
{

}

BoolMC::~BoolMC(void)
{

}

void BoolMC::setVolumeDimensions(int x, int y, int z)
{
	m_volumeDimX = x;
	m_volumeDimY = y;
	m_volumeDimZ = z;
	m_volumeStepYZ = y * z;
	m_volumeStepXY = x * y;
}

void BoolMC::setZstepSizeAndOrthoSize(float zStepSizeIn, float orthoSizeIn)
{
	m_orthoSize = orthoSizeIn * 10000;
	m_stepSizeZ = zStepSizeIn * 10000;
}

void BoolMC::calcStepSizeXY()
{
	m_stepSize = (m_orthoSize / (float)min(m_volumeDimY, m_volumeDimX));
	m_stepSizeHalf = m_stepSize / 2;
}

//code is based on: http://paulbourke.net/geometry/polygonise/
void BoolMC::polygonize(std::vector<bool>* vaVec)
{
	m_edgeTable = new int[256] {
		0x0, 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
			0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
			0x190, 0x99, 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
			0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
			0x230, 0x339, 0x33, 0x13a, 0x636, 0x73f, 0x435, 0x53c,
			0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
			0x3a0, 0x2a9, 0x1a3, 0xaa, 0x7a6, 0x6af, 0x5a5, 0x4ac,
			0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
			0x460, 0x569, 0x663, 0x76a, 0x66, 0x16f, 0x265, 0x36c,
			0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
			0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff, 0x3f5, 0x2fc,
			0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
			0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55, 0x15c,
			0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
			0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc,
			0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
			0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
			0xcc, 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
			0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
			0x15c, 0x55, 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
			0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
			0x2fc, 0x3f5, 0xff, 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
			0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
			0x36c, 0x265, 0x16f, 0x66, 0x76a, 0x663, 0x569, 0x460,
			0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
			0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa, 0x1a3, 0x2a9, 0x3a0,
			0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
			0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33, 0x339, 0x230,
			0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
			0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99, 0x190,
			0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
			0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0 };

	m_triTable = new int[256 * 16] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1,
		3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1,
		3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1,
		3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1,
		9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1,
		9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
		2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1,
		8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1,
		9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
		4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1,
		3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1,
		1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1,
		4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1,
		4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1,
		9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
		5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1,
		2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1,
		9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
		0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
		2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1,
		10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1,
		4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1,
		5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1,
		5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1,
		9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1,
		0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1,
		1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1,
		10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1,
		8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1,
		2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1,
		7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1,
		9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1,
		2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1,
		11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1,
		9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1,
		5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1,
		11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1,
		11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
		1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1,
		9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1,
		5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1,
		2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
		0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
		5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1,
		6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1,
		3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1,
		6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1,
		5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1,
		1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
		10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1,
		6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1,
		8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1,
		7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1,
		3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
		5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1,
		0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1,
		9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1,
		8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1,
		5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1,
		0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1,
		6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1,
		10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1,
		10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1,
		8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1,
		1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1,
		3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1,
		0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1,
		10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1,
		3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1,
		6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1,
		9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1,
		8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1,
		3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1,
		6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1,
		0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1,
		10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1,
		10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1,
		2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1,
		7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1,
		7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1,
		2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1,
		1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1,
		11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1,
		8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1,
		0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1,
		7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
		10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
		2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
		6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1,
		7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1,
		2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1,
		1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1,
		10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1,
		10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1,
		0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1,
		7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1,
		6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1,
		8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1,
		9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1,
		6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1,
		4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1,
		10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1,
		8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1,
		0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1,
		1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1,
		8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1,
		10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1,
		4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1,
		10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
		5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
		11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1,
		9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
		6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1,
		7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1,
		3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1,
		7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1,
		9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1,
		3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1,
		6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1,
		9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1,
		1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1,
		4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1,
		7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1,
		6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1,
		3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1,
		0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1,
		6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1,
		0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1,
		11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1,
		6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1,
		5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1,
		9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1,
		1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1,
		1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1,
		10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1,
		0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1,
		5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1,
		10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1,
		11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1,
		9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1,
		7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1,
		2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1,
		8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1,
		9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1,
		9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1,
		1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1,
		9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1,
		9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1,
		5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1,
		0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1,
		10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1,
		2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1,
		0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1,
		0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1,
		9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1,
		5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1,
		3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1,
		5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1,
		8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1,
		0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1,
		9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1,
		0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1,
		1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1,
		3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1,
		4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1,
		9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1,
		11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1,
		11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1,
		2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1,
		9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1,
		3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1,
		1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1,
		4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1,
		4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1,
		0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1,
		3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1,
		3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1,
		0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1,
		9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1,
		1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
	};

	std::cout << "---> Prepare multithread execution of boolean marching cubes" << std::endl;
	//IMPORTANT TODO: Make it ready for real division of tasks depending on numOfCore
	//Get Numbers of cores - max cores/threads supported in this application: 8
	//unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
	//TODO: At this time, there will be 8 threads - better is checking with hardware_concurrency the number of cores and manage from self
	//TODO: Use OpenMP!
	m_verticesThread0 = new osg::Vec3Array;
	m_verticesThread1 = new osg::Vec3Array;
	m_verticesThread2 = new osg::Vec3Array;
	m_verticesThread3 = new osg::Vec3Array;
	m_verticesThread4 = new osg::Vec3Array;
	m_verticesThread5 = new osg::Vec3Array;
	m_verticesThread6 = new osg::Vec3Array;
	m_verticesThread7 = new osg::Vec3Array;

	m_normalsThread0 = new osg::Vec3Array;
	m_normalsThread1 = new osg::Vec3Array;
	m_normalsThread2 = new osg::Vec3Array;
	m_normalsThread3 = new osg::Vec3Array;
	m_normalsThread4 = new osg::Vec3Array;
	m_normalsThread5 = new osg::Vec3Array;
	m_normalsThread6 = new osg::Vec3Array;
	m_normalsThread7 = new osg::Vec3Array;

	unsigned int coreStep = (int)m_volumeDimZ / 8;

	unsigned int start0 = 0;					//0
	unsigned int end0 = coreStep + 1;			//6

	unsigned int start1 = end0 - 1;				//5
	unsigned int end1 = start1 + coreStep + 1;	//11

	unsigned int start2 = end1 - 1;				//10
	unsigned int end2 = start2 + coreStep + 1;	//16

	unsigned int start3 = end2 - 1;				//15
	unsigned int end3 = start3 + coreStep + 1;	//21

	unsigned int start4 = end3 - 1;				//0
	unsigned int end4 = start4 + coreStep + 1;	//6

	unsigned int start5 = end4 - 1;				//5
	unsigned int end5 = start5 + coreStep + 1;	//11

	unsigned int start6 = end5 - 1;				//10
	unsigned int end6 = start6 + coreStep + 1;	//16

	unsigned int start7 = end6 - 1;				//15
	unsigned int end7 = start7 + coreStep;		//21

	std::cout << "---> multithread execution of Boolean Marching Cubes starts" << std::endl;
	std::thread thread0(&BoolMC::coreBMC, this, start0, end0, m_verticesThread0, m_normalsThread0, vaVec);
	std::thread thread1(&BoolMC::coreBMC, this, start1, end1, m_verticesThread1, m_normalsThread1, vaVec);
	std::thread thread2(&BoolMC::coreBMC, this, start2, end2, m_verticesThread2, m_normalsThread2, vaVec);
	std::thread thread3(&BoolMC::coreBMC, this, start3, end3, m_verticesThread3, m_normalsThread3, vaVec);
	std::thread thread4(&BoolMC::coreBMC, this, start4, end4, m_verticesThread4, m_normalsThread4, vaVec);
	std::thread thread5(&BoolMC::coreBMC, this, start5, end5, m_verticesThread5, m_normalsThread5, vaVec);
	std::thread thread6(&BoolMC::coreBMC, this, start6, end6, m_verticesThread6, m_normalsThread6, vaVec);
	std::thread thread7(&BoolMC::coreBMC, this, start7, end7, m_verticesThread7, m_normalsThread7, vaVec);

	thread0.join();
	thread1.join();
	thread2.join();
	thread3.join();
	thread4.join();
	thread5.join();
	thread6.join();
	thread7.join();

	std::cout << "---> multithread execution ends. Joining thread data" << std::endl;
	//JOIN all vertices
	osg::Vec3Array* vertices = new osg::Vec3Array;
	vertices->insert(vertices->end(), m_verticesThread0->begin(), m_verticesThread0->end());
	vertices->insert(vertices->end(), m_verticesThread1->begin(), m_verticesThread1->end());
	vertices->insert(vertices->end(), m_verticesThread2->begin(), m_verticesThread2->end());
	vertices->insert(vertices->end(), m_verticesThread3->begin(), m_verticesThread3->end());
	vertices->insert(vertices->end(), m_verticesThread4->begin(), m_verticesThread4->end());
	vertices->insert(vertices->end(), m_verticesThread5->begin(), m_verticesThread5->end());
	vertices->insert(vertices->end(), m_verticesThread6->begin(), m_verticesThread6->end());
	vertices->insert(vertices->end(), m_verticesThread7->begin(), m_verticesThread7->end());

	osg::Vec3Array* normals = new osg::Vec3Array;
	normals->insert(normals->end(), m_normalsThread0->begin(), m_normalsThread0->end());
	normals->insert(normals->end(), m_normalsThread1->begin(), m_normalsThread1->end());
	normals->insert(normals->end(), m_normalsThread2->begin(), m_normalsThread2->end());
	normals->insert(normals->end(), m_normalsThread3->begin(), m_normalsThread3->end());
	normals->insert(normals->end(), m_normalsThread4->begin(), m_normalsThread4->end());
	normals->insert(normals->end(), m_normalsThread5->begin(), m_normalsThread5->end());
	normals->insert(normals->end(), m_normalsThread6->begin(), m_normalsThread6->end());
	normals->insert(normals->end(), m_normalsThread7->begin(), m_normalsThread7->end());


	std::cout << "--> preparing data for STL export... " << std::endl;
	osg::Group* root = new osg::Group();

	std::stringstream argv1;
	argv1 << m_argv[1];
	std::string showPreview = argv1.str();

	osg::ref_ptr<osg::Geode> geodePoints = new osg::Geode;
	////Render points as a cloud  - Start Comment
	////Dont uncomment for huge data sets. Will be very slow. This is only for debug Viz. Max is about 128x128x128
	////############################
	//if (showPreview.compare("true") == 0 || showPreview.compare("1") == 0)
	//{
	//	osg::Vec3Array* verticesAsPoints = new osg::Vec3Array;
	//	osg::Vec4Array* colorsPoints = new osg::Vec4Array;
	//	for (unsigned int z = 0; z < m_volumeDimZ; z++)
	//	{
	//		for (unsigned int y = 0; y < m_volumeDimY; y++)
	//		{
	//			for (unsigned int x = 0; x < m_volumeDimX; x++)
	//			{
	//				verticesAsPoints->push_back(osg::Vec3(x * m_stepSize, y * m_stepSize, z * m_stepSizeZ));
	//				//if (volume[x * dimension + y * dimension + z])
	//				if (vaVec->at(z * m_volumeStepXY + y * m_volumeDimX + x))
	//				{
	//					colorsPoints->push_back(osg::Vec4(0.1f, 0.9f, 0.1f, 1.0f));
	//					//std::cout << "TRUE +++ GRUEN an Stelle:" << x << " " << y << " " << z << std::endl;
	//				}
	//				else
	//				{
	//					colorsPoints->push_back(osg::Vec4(0.9f, 0.1f, 0.1f, 1.0f));
	//					//std::cout << "FALSE ---  ROT an Stelle:" << x << " " << y << " " << z << std::endl;
	//				}
	//			}
	//		}
	//	}
	//	osg::ref_ptr<osg::Geometry> geometryPoints = new osg::Geometry;
	//	geometryPoints->setVertexArray(verticesAsPoints);
	//	osg::ref_ptr<osg::Vec3Array> normalArrayPoints = new osg::Vec3Array;
	//	for (int i = 0; verticesAsPoints->size() >= i; i++)
	//	{
	//		normalArrayPoints->push_back(osg::Vec3(1.0f, 0.0f, 0.0f));
	//	}
	//	geometryPoints->setNormalArray(normalArrayPoints);
	//	geometryPoints->setNormalBinding(osg::Geometry::BIND_OVERALL);
	//	geometryPoints->setColorArray(colorsPoints);
	//	geometryPoints->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	//	geodePoints->addDrawable(geometryPoints);
	//	osg::PolygonMode* polymode = new osg::PolygonMode;
	//	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::POINT);
	//	geodePoints->getOrCreateStateSet()->setAttributeAndModes(polymode, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//	geodePoints->getOrCreateStateSet()->setAttributeAndModes(new osg::Point(0.001f), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//	geodePoints->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	//	geometryPoints->addPrimitiveSet(new osg::DrawArrays(GL_POINTS, 0, verticesAsPoints->size()));
	//	root->addChild(geodePoints);
	//}
	//############################
	//Render point Cloud End Comment

	





	//DAS sollte CUDA machen!!!!!!!!!!!
	osg::DrawElementsUInt* triangles = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
	//for (int i = 0; i < vertices->size(); i++)
	//{
	//	triangles->push_back(i);
	//}
	triangles->resize(vertices->size());
	std::cout << "Before Thrust numbers of Vertes:" << vertices->size() << std::endl;
	osg::Vec3Array* verticesTemp = new osg::Vec3Array;
	verticesTemp->assign(vertices->begin(), vertices->begin() + vertices->size());

	std::sort(vertices->begin(), vertices->end());

	// find unique vertices and erase redundancies
	vertices->erase(std::unique(vertices->begin(), vertices->end()), vertices->end());
	
	testRun(vertices, verticesTemp, triangles);
	std::cout << "After Thrust numbers of Vertes:" << vertices->size() << std::endl;

	//Some ideas for implementing lower_bound with an vectorized outpot, but it's not clear how to finish it
	//for (unsigned int l = 0; l < verticesTemp->size(); ++l)
	//{
	//	//L539 must be commented for this

	//	triangles->push_back(*std::lower_bound(vertices->begin(), vertices->end(), verticesTemp->at(l)));
	//}


	m_geometry = new osg::OpenMeshGeometry(osg::Vec4f(1.0, 1.0, 0.0, 0.0), osg::Vec4f(0.0, 0.0, 0.0, 0.0));

	m_geometry->setVertexArray(vertices);


	//m_geometry->addPrimitiveSet(dynamic_cast<osg::PrimitiveSet*>(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, vertices->size())));
	m_geometry->addPrimitiveSet(triangles);
	//geometry->setNormalArray(normals, osg::Array::BIND_PER_VERTEX);
	//m_geometry->flipNormals();
	//m_geometry->getNormalArray()->dirty();
	//osg::Vec4Array* colors = new osg::Vec4Array;
	//for (int i = 0; vertices->size() > i; i++)
	//{
	//	colors->push_back(osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f));
	//}
	//m_geometry->setColorArray(colors);
	//m_geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	m_surfaceGeode = new osg::Geode();
	m_surfaceGeode->getOrCreateStateSet();
	m_surfaceGeode->addDrawable(m_geometry.get());

	////m_geometry->removeDoubles();
	//////Reduce some (not all!) double vertices ->it about 25-30% reducing rate - possible are about 70-75%
	//osgUtil::IndexMeshVisitor imv;
	//m_surfaceGeode->accept(imv);
	//imv.makeMesh();
	//


	//root->addChild(m_surfaceGeode);


	//###Decimeter Start###
	m_geometry->removeSeparatedObjects();


	//### Export as STL###
	//TODO: Get it back to main function -class function is finished
	std::string safePath = m_argv[2];
	std::string fn = m_argv[2];

	if (fn.substr(fn.find_last_of(".") + 1) == "fexbit")
		safePath.resize(safePath.size() - 6);
	else
		safePath.resize(safePath.size() - 3); //pcl, fex and xml have only 3 chars


	//########################################
	//Choose here the Export Format
	int fileFormat = 0; //0 = STL
						//1 = OBJ

	std::stringstream finalStlSafePath;
	if (fileFormat == 0){
		std::cout << "--> Exporting to STL... " << std::endl;
		finalStlSafePath << safePath << "stl";
	}
	else if (fileFormat == 1){
		std::cout << "--> Exporting to OBJ... " << std::endl;
		finalStlSafePath << safePath << "obj";
	}

	m_geometry->writeFileToDisk(finalStlSafePath.str());

	//Old slow osg export:
	//osgDB::writeNodeFile(*(root), finalStlSafePath.str());
	//std::cout << "--> STL export finished... " << std::endl;
	//std::cout << " " << std::endl;
	//std::cout << "#################################################### " << std::endl;
	//std::cout << "--> Fexbit to raw STL successfully translated! " << std::endl;
	//std::cout << " " << std::endl;

	osgViewer::Viewer* gMainViewer = new osgViewer::Viewer();
	osgViewer::Viewer* gMainViewer2 = new osgViewer::Viewer();
	if (showPreview.compare("true") == 0 || showPreview.compare("1") == 0)
	{
		std::cout << "--> Starting OSGViewer" << std::endl;
		gMainViewer->setUpViewInWindow(250, 250, 800, 600, 0);
		gMainViewer->setSceneData(root);

		gMainViewer->setCameraManipulator(new osgGA::TrackballManipulator());

		gMainViewer->getCameraManipulator()->setAutoComputeHomePosition(true);

		gMainViewer->getCamera()->setClearColor(osg::Vec4(0.0, 0.2, 0.2, 1.0));

		gMainViewer->realize();

		gMainViewer->getCameraManipulator()->home(1.0);
		gMainViewer->addEventHandler(new osgViewer::StatsHandler);
		gMainViewer->addEventHandler(new osgGA::StateSetManipulator(gMainViewer->getCamera()->getOrCreateStateSet()));
	}

	std::stringstream meshLabCommands;
	meshLabCommands << " " << finalStlSafePath.str() << " " << finalStlSafePath.str() << " " << "meshLabScript1.mlx";
	//ShellExecute(0, "open", "start_meshlab.bat", meshLabCommands.str().c_str(), 0, SW_SHOW);
	std::cout << "" << std::endl;
	std::cout << "--> MESHLAB PROCESSING WILL START NOW IN A NEW WINDOW" << std::endl;
	std::cout << "--> MESHLAB WILL NOW ADD THE FILTERS WHICH ARE IN meshlab/meshLabScript1.mlx" << std::endl;
	std::cout << "--> WAIT UNTIL TEXT TURN TO GREEN IN NEW WINDOW" << std::endl;

	if (showPreview.compare("true") == 0 || showPreview.compare("1") == 0)
	{
		createOriginGeometry(root);
		gMainViewer->run();
	}
	else{
		//system("PAUSE");
	}
}

//The core of the core
void BoolMC::coreBMC(unsigned int start, unsigned int end, osg::Vec3Array* verticesThreadIn, osg::Vec3Array* normalsThreadIn, std::vector<bool>* vaVec)
{
	osg::ref_ptr<osg::Vec3Array> vertlist = new osg::Vec3Array;
	vertlist->resize(12);

	//NOTICE: Important to get the array in right way ->from Z to X and consider right volume steps! This here works fine (after messing days with this)
	for (unsigned int z = start; z < end - 1; z++)
	{
		for (unsigned int y = 0; y < m_volumeDimY - 1; y++)
		{
			for (unsigned int x = 0; x < m_volumeDimX - 1; x++)
			{
				int cubeindex = 0;
				if (vaVec->at(z * m_volumeStepXY + y * m_volumeDimX + x))
					cubeindex |= 1;

				if (vaVec->at(z * m_volumeStepXY + y * m_volumeDimX + x + 1))
					cubeindex |= 2;

				if (vaVec->at(z * m_volumeStepXY + (y + 1) * m_volumeDimX + x + 1))
					cubeindex |= 4;

				if (vaVec->at(z * m_volumeStepXY + (y + 1) * m_volumeDimX + x))
					cubeindex |= 8;

				if (vaVec->at((z + 1) * m_volumeStepXY + y * m_volumeDimX + x))
					cubeindex |= 16;

				if (vaVec->at((z + 1) * m_volumeStepXY + y * m_volumeDimX + x + 1))
					cubeindex |= 32;

				if (vaVec->at((z + 1) * m_volumeStepXY + (y + 1) * m_volumeDimX + x + 1))
					cubeindex |= 64;

				if (vaVec->at((z + 1) *  m_volumeStepXY + (y + 1)  * m_volumeDimX + x))
					cubeindex |= 128;


				/*int cubeindex = 0;
				if (vaVec->at(z * m_volumeStepXY + y * m_volumeDimX + x))
				cubeindex |= 1;

				if (vaVec->at((z + 1) * m_volumeStepXY + y * m_volumeDimX + x))
				cubeindex |= 2;

				if (vaVec->at((z + 1) * m_volumeStepXY + y * m_volumeDimX + x))
				cubeindex |= 4;

				if (vaVec->at(z * m_volumeStepXY + (y + 1) * m_volumeDimX + x))
				cubeindex |= 8;

				if (vaVec->at(z * m_volumeStepXY + y * m_volumeDimX + (x + 1)))
				cubeindex |= 16;

				if (vaVec->at((z + 1) * m_volumeStepXY + y * m_volumeDimX + (x + 1)))
				cubeindex |= 32;

				if (vaVec->at((z + 1) * m_volumeStepXY + (y + 1) * m_volumeDimX + (x + 1)))
				cubeindex |= 64;

				if (vaVec->at(z *  m_volumeStepXY + (y + 1)  * m_volumeDimX + (x + 1)))
				cubeindex |= 128;*/


				/* Cube is entirely in/out of the surface */
				if (m_edgeTable[cubeindex] & 0)
				{
					break;
				}
				/* Find the vertices where the surface intersects the cube */
				if (m_edgeTable[cubeindex] & 1)
				{
					//0
					vertlist->at(0).set(osg::Vec3d(x * m_stepSize + m_stepSizeHalf, y * m_stepSize, z * m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 2)
				{
					//1
					vertlist->at(1).set(osg::Vec3d(x * m_stepSize + m_stepSize, y * m_stepSize + m_stepSizeHalf, z * m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 4)
				{
					//2
					vertlist->at(2).set(osg::Vec3d(x * m_stepSize + m_stepSizeHalf, y * m_stepSize + m_stepSize, z * m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 8)
				{
					//3
					vertlist->at(3).set(osg::Vec3d(x * m_stepSize, y * m_stepSize + m_stepSizeHalf, z * m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 16)
				{
					//4
					vertlist->at(4).set(osg::Vec3d(x * m_stepSize + m_stepSizeHalf, y * m_stepSize, z * m_stepSizeZ + m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 32)
				{
					//5
					vertlist->at(5).set(osg::Vec3d(x * m_stepSize + m_stepSize, y * m_stepSize + m_stepSizeHalf, z * m_stepSizeZ + m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 64)
				{
					//6
					vertlist->at(6).set(osg::Vec3d(x * m_stepSize + m_stepSizeHalf, y * m_stepSize + m_stepSize, z * m_stepSizeZ + m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 128)
				{
					//7
					vertlist->at(7).set(osg::Vec3d(x * m_stepSize, y * m_stepSize + m_stepSizeHalf, z * m_stepSizeZ + m_stepSizeZ));
				}
				if (m_edgeTable[cubeindex] & 256)
				{
					//8
					vertlist->at(8).set(osg::Vec3d(x * m_stepSize, y * m_stepSize, z * m_stepSizeZ + m_stepSizeZ / 2));
				}
				if (m_edgeTable[cubeindex] & 512)
				{
					//9
					vertlist->at(9).set(osg::Vec3d(x * m_stepSize + m_stepSize, y * m_stepSize, z * m_stepSizeZ + m_stepSizeZ / 2));
				}
				if (m_edgeTable[cubeindex] & 1024)
				{
					//10
					vertlist->at(10).set(osg::Vec3d(x * m_stepSize + m_stepSize, y * m_stepSize + m_stepSize, z * m_stepSizeZ + m_stepSizeZ / 2));
				}
				if (m_edgeTable[cubeindex] & 2048)
				{
					//11
					vertlist->at(11).set(osg::Vec3d(x * m_stepSize, y * m_stepSize + m_stepSize, z * m_stepSizeZ + m_stepSizeZ / 2));
				}

				for (int i = 0; m_triTable[cubeindex * 16 + i] != -1; i += 3) {
					//std::cout << "Ausgabe von triTable: " << m_triTable[cubeindex * 16 + i] << std::endl;
					//std::cout << "Ausgabe von triTable: " << m_triTable[cubeindex * 16 + i + 1] << std::endl;
					//std::cout << "Ausgabe von triTable: " << m_triTable[cubeindex * 16 + i + 2] << std::endl;
					verticesThreadIn->push_back(vertlist->at(m_triTable[cubeindex * 16 + i]));
					verticesThreadIn->push_back(vertlist->at(m_triTable[cubeindex * 16 + i + 1]));
					verticesThreadIn->push_back(vertlist->at(m_triTable[cubeindex * 16 + i + 2]));

					//osg::Vec3f test = osg::Vec3f((vertlist->at(m_triTable[cubeindex * 16 + i + 1]) - vertlist->at(m_triTable[cubeindex * 16 + i])) ^ (vertlist->at(m_triTable[cubeindex * 16 + i + 2]) - vertlist->at(m_triTable[cubeindex * 16 + i + 1])));
					osg::Vec3f normal = osg::Vec3f((verticesThreadIn->at(verticesThreadIn->size() - 2) - verticesThreadIn->at(verticesThreadIn->size() - 3)) ^ (verticesThreadIn->at(verticesThreadIn->size() - 1) - verticesThreadIn->at(verticesThreadIn->size() - 2)));
					normal.normalize();
					//NOTICE: MeshLab handle normals as inverted. Blender are fine with inverted
					normalsThreadIn->push_back(-normal);
					normalsThreadIn->push_back(-normal);
					normalsThreadIn->push_back(-normal);
					//std::cout << "Normal: " << test.x() << " " << test.y() << " " << test.z() << std::endl;
				}
			}
		}
	}
}

//TODO: Graveyard?
//Experimental function to calc the gradient and the normal - like in the original MC Paper
osg::Vec3f BoolMC::calcNormal(int x, int y, int z)
{
	int xNormal = m_volume[x][y][z] - m_volume[x - 1][y][z];
	int yNormal = m_volume[x][y][z] - m_volume[x][y - 1][z];
	int zNormal = m_volume[x][y][z] - m_volume[x][y][z - 1];
	osg::Vec3f vertNormal = osg::Vec3f((float)xNormal, (float)yNormal, (float)zNormal);
	vertNormal.normalize();
	return vertNormal;
}

//Only for debug Viz. Will be called with the commented point cloud stuff above
void BoolMC::createOriginGeometry(osg::Group* rootIn){

	osg::PositionAttitudeTransform* m_originVisTransform = new osg::PositionAttitudeTransform();

	//double s = 0.05;
	float originScale = 0.01;
	m_originVisTransform->setScale(osg::Vec3d(originScale, originScale, originScale));

	float height = 1.0;
	float rad = 0.1;

	//- - - x-Axis - - - 
	osg::Cylinder *cylinderX = new osg::Cylinder(osg::Vec3(0.5 * height, 0.0, 0.0), rad, height);
	cylinderX->setRotation(osg::Quat(osg::PI_2, osg::Vec3(0.0, 1.0, 0.0)));
	osg::ShapeDrawable *cylinderXDrawable = new osg::ShapeDrawable(cylinderX);
	cylinderXDrawable->setColor(osg::Vec4(1.0, 0.0, 0.0, 1.0));

	osg::Geode* cylXGeode = new osg::Geode;
	cylXGeode->addDrawable(cylinderXDrawable);

	cylXGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_originVisTransform->addChild(cylXGeode);



	//- - - y-Axis - - - 
	osg::Cylinder *cylinderY = new osg::Cylinder(osg::Vec3(0.0, 0.5 * height, 0.0), rad, height);
	cylinderY->setRotation(osg::Quat(osg::PI_2, osg::Vec3(1.0, 0.0, 0.0)));
	osg::ShapeDrawable *cylinderYDrawable = new osg::ShapeDrawable(cylinderY);
	cylinderYDrawable->setColor(osg::Vec4(0.0, 1.0, 0.0, 1.0));

	osg::Geode* cylYGeode = new osg::Geode;
	cylYGeode->addDrawable(cylinderYDrawable);

	cylYGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_originVisTransform->addChild(cylYGeode);


	//- - - z-Axis - - - 
	osg::Cylinder *cylinderZ = new osg::Cylinder(osg::Vec3(0.0, 0.0, 0.5 * height), rad, height);
	//cylinderZ->setRotation(osg::Quat(osg::PI_2, osg::Vec3(1.0, 0.0, 0.0)));
	osg::ShapeDrawable *cylinderZDrawable = new osg::ShapeDrawable(cylinderZ);
	cylinderZDrawable->setColor(osg::Vec4(0.0, 0.0, 1.0, 1.0));

	osg::Geode* cylZGeode = new osg::Geode;
	cylZGeode->addDrawable(cylinderZDrawable);

	cylZGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_originVisTransform->addChild(cylZGeode);

	rootIn->addChild(m_originVisTransform);
}