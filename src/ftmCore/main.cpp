//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################

//############################################################################################
//Terms used in this soruce:
//FIXME: means, that something can be written in better code and should changed in future
//TODO: means, that there are more to do but it is not compelling
//EXP: stands for Explanation. It explains code
//OBSCURE: stands for obscure behaviour of the programm and the reason is not known
//############################################################################################

#include "InputManager.h"
#include "OutputManager.h"
#include "BoolMC.h"
#include <iostream>

//Choose here the Export Format
int m_fileFormat = 0;	//0 = STL
						//1 = OBJ

int main(int argc, char** argv)
{
	std::cout << "### Welcome to the fractalToMesh Converter ###" << std::endl;
	std::cout << "### Philipp Ladwig - HS Duesseldorf 2015 ### \n" << std::endl;

	ftm::InputManager* inputManager = new ftm::InputManager();
	ftm::OutputManager* outputManager = new ftm::OutputManager();
	std::vector<float>* vaVec = new std::vector<float>;

	//### Determine file extension and read file with approriate function
	std::string filename = argv[2];
	if (filename.substr(filename.find_last_of(".") + 1) == "fexbit")
	{
		std::cout << "--> fexbit will be read" << std::endl;
		inputManager->loadFEXBIT(argc, argv);

		//Create BoolMarching Cubes processing object
		ftm::BoolMC* boolMC = new ftm::BoolMC(argc, argv);

		//Set voxel resolution
		boolMC->setVolumeDimensions(inputManager->getVolumeDimX(), inputManager->getVolumeDimY(), inputManager->getVolumeDimZ());

		//Z and ortho must be set, before calcStepSizeXY will be called
		boolMC->setZstepSizeAndOrthoSize(inputManager->getZstepSize(), inputManager->getOrthoSize());
		boolMC->calcStepSizeXY();

		//"Triangulation" of point cloud
		boolMC->polygonize(inputManager->getBoolArray());
		//Before this line its stucks in the polygonize()

		//boolMC->getOpenMeshGeometry()->removeDoubles();

		//Export preperation
		//std::string safePath = argv[2];
		//safePath.resize(safePath.size() - 6);

		//std::stringstream finalStlSafePath;
		//if (m_fileFormat == 0){
		//	std::cout << "--> Exporting to STL... " << std::endl;
		//	finalStlSafePath << safePath << "stl";
		//}
		//else if (m_fileFormat == 1){
		//	std::cout << "--> Exporting to OBJ... " << std::endl;
		//	finalStlSafePath << safePath << "obj";
		//}

		//FIXME: Can't export with OpenMesh, when I export in outputManager. When I export in OpenMeshGeoemtry, there are no problems at all... Additional to this, I cant use OM61, because it seems, there is an bug, with vector_cast() ->it requires 3 parameter, but in ExporterT is only one given
		//outputManager->writeFileToDisk(boolMC->getGeometryResult(), finalStlSafePath.str());
		return 0;
	}
	else
	{
		std::cout << "ERROR: Only .fexbit files will be read. Fexbit is a file format by Okan Koese which will be used for exchange data from 3DFractalScanner at least Version 0.8 with koesisch Coding." << std::endl;
	}
	return (0);
}