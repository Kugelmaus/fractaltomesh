//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################
#include "InputManager.h"

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/TrackballManipulator>

#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <boost/phoenix/phoenix.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

//NOTICE: Nasty Hack ->Must define globals, cause spirit seems to have problems with calling members per reference....cant say way..maybe I miss something?
namespace qi = boost::spirit::qi;
unsigned int g_multiplicator = 0;
std::vector<bool>* g_vaVec = new std::vector<bool>;
unsigned int g_xResolution = 512;
unsigned int g_yResolution = 512;
unsigned int g_zResolution = 512;
float g_zStepSize = 0.1;
float g_orthoSize = 2.0;

using namespace ftm;

#if 1
struct float3
{
	float x, y, z;
};

BOOST_FUSION_ADAPT_STRUCT(float3, (float, x)(float, y)(float, z))
//typedef std::vector<float3> data_t;
typedef std::vector<float> data_t;
#else
// there is no speed difference when parsing into a straight container
// (only works with the spirit implementation)
typedef std::vector<float> data_t;
#endif

InputManager::InputManager()
{
}

InputManager::~InputManager(void)
{
}
	
//Fex with k�sisch coding
void InputManager::loadFEXBIT(int argc, char** argv)
{
#if 0
	std::cin.unsetf(std::ios::skipws);
	std::istreambuf_iterator<char> f_(std::cin), l_;

	const std::vector<char> v(f_, l_);
	auto f = v.data(), l = f + v.size();
#elif 1
	boost::iostreams::mapped_file mmap(argv[2], boost::iostreams::mapped_file::readonly);
	auto f = mmap.const_data();
	auto l = f + mmap.size();
#endif
#if 1
	using namespace qi;
	using boost::phoenix::ref;
	using qi::double_;
	using qi::phrase_parse;
	using qi::_1;
	using ascii::space;
	using boost::phoenix::push_back;
	//bool ok = parse(f, l, (int_ >> '-' >> int_ >> '-' >> int_[boost::bind(&InputManager::fex2Resolution, this, _1)] | int_[boost::bind(&InputManager::fex2Param1, this, _1)] >> '_' >> int_[boost::bind(&InputManager::fex2Param2, this, _1)] | int_[boost::bind(&InputManager::fex2Param3, this, _1)]) % ' ' % eol);
	bool ok = parse(f, l, ('-' >> int_[&fexBit_xResolution] 
		>> '-' >> int_[&fexBit_yResolution] 
		>> '-' >> int_[&fexBit_zResolution] 
		>> '-' >> float_[&fexBit_zStepSize] 
		>> '-' >> float_[&fexBit_orthoSize] |
		int_[&fex2Param1] >> '_' >> int_[&fex2Param2] |
		int_[&fex2Param3]) % ' ' % eol);

	if (f != l)
		std::cout << "--> spirit runs not to the end of file!\n";
	if (ok)
		std::cout << "--> parse success\n";

	m_volumeDimX = g_xResolution;
	m_volumeDimY = g_yResolution;
	m_volumeDimZ = g_zResolution;
	m_zStepSize = g_zStepSize;
	m_orthoSize = g_orthoSize;
	
	//return g_vaVec;
	
#elif 1
	errno = 0;
	char* next = nullptr;
	float3 tmp;
	while (errno == 0 && f && f<(l - 12)) {
		tmp.x = strtod(f, &next); f = next;
		tmp.y = strtod(f, &next); f = next;
		tmp.z = strtod(f, &next); f = next;
		data.push_back(tmp);
	}
#else
	FILE* file = fopen("input.txt", "r");
	if (NULL == file) {
		printf("Failed to open 'input.txt'");
		return 255;
	}
	float3 tmp;
	do {
		int nItemsRead = fscanf(file, "%f %f %f\n", &tmp.x, &tmp.y, &tmp.z);
		if (3 != nItemsRead)
			break;
		data.push_back(tmp);
	} while (1);

#endif
}

void InputManager::fex2Param1(int const& in)
{
	//std::cout << "Ausgabe!!! Func" << in << std::endl;
	g_multiplicator = in;
}

void InputManager::fex2Param2(int const& in)
{
	//std::cout << "Ausgabe!!!Von test2" << std::endl;
	for (auto i = 0; i < g_multiplicator; i++)
	{
		g_vaVec->push_back(in == 0 ? false : true);
	}

	//New better RunLength Decoder Version without underline
	float inOrOut = in % 10;
	//or better?:
	//bool inOrOut = in % 10;
	if (inOrOut == 0)
	{

	}


}

void InputManager::fex2Param3(int const& in)
{
	g_vaVec->push_back(in == 0 ? false : true);
}

void InputManager::fexBit_xResolution(int const& in)
{
	//std::cout << "Resolution ist:" << in << std::endl;
	g_xResolution = in;
	std::cout << "--> x Volume Resolution: " << g_xResolution << "\n";
}
void InputManager::fexBit_yResolution(int const& in)
{
	//std::cout << "Resolution ist:" << in << std::endl;
	g_yResolution = in;
	std::cout << "--> y Volume Resolution: " << g_yResolution << "\n";
}
void InputManager::fexBit_zResolution(int const& in)
{
	//std::cout << "Resolution ist:" << in << std::endl;
	g_zResolution = in;
	std::cout << "--> z Volume Resolution: " << g_zResolution << "\n";
}
void InputManager::fexBit_zStepSize(float const& in)
{
	g_zStepSize = in;
	std::cout << "--> z Step Size: " << g_zStepSize << "\n";
	std::cout << "--> read file...\n";
}

void InputManager::fexBit_orthoSize(float const& in)
{
	g_orthoSize = in;
}


std::vector<bool>* InputManager::getBoolArray()
{
	return g_vaVec;
}