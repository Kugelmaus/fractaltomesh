//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################
#pragma once

#include <osg/Geode>
#include "OpenMeshGeometry.h"

namespace ftm{
	class BoolMC
	{
		// #### CONSTRUCTOR & DESTRUCTOR ###############
	public:
		BoolMC(int argc, char** argv);
		~BoolMC(void);


		// #### MEMBER VARIABLES ###############
	private:
		osg::ref_ptr<osg::OpenMeshGeometry> m_geometry;
		osg::Geode* m_surfaceGeode;
		int* m_edgeTable = new int[256];
		int* m_triTable = new int[256 * 16];
		double m_stepSize;
		double m_stepSizeHalf;
		double m_stepSizeZ;
		double m_stepSize_Z_Half;
		std::vector<std::vector<std::vector<bool> > > m_volume;
		unsigned int m_counter;
		int m_argc;
		char** m_argv;
		unsigned int m_volumeDimX;
		unsigned int m_volumeDimY;
		unsigned int m_volumeDimZ;
		unsigned int m_volumeStepYZ;
		unsigned int m_volumeStepXY;
		//float m_volumeStepZ;
		double m_orthoSize;


		osg::Vec3Array* m_verticesThread0;
		osg::Vec3Array* m_verticesThread1;
		osg::Vec3Array* m_verticesThread2;
		osg::Vec3Array* m_verticesThread3;
		osg::Vec3Array* m_verticesThread4;
		osg::Vec3Array* m_verticesThread5;
		osg::Vec3Array* m_verticesThread6;
		osg::Vec3Array* m_verticesThread7;

		osg::Vec3Array* m_normalsThread0;
		osg::Vec3Array* m_normalsThread1;
		osg::Vec3Array* m_normalsThread2;
		osg::Vec3Array* m_normalsThread3;
		osg::Vec3Array* m_normalsThread4;
		osg::Vec3Array* m_normalsThread5;
		osg::Vec3Array* m_normalsThread6;
		osg::Vec3Array* m_normalsThread7;
		std::vector<bool> vaVec2;

		// #### MEMBER FUNCTIONS ###############
	private:
		void coreBMC(unsigned int start, unsigned int end, osg::Vec3Array* m_verticesThreadIn, osg::Vec3Array* m_normalsThreadIn, std::vector<bool>* vaVec);
		osg::Vec3f calcNormal(int x, int y, int z);

	public:
		void setVolumeDimensions(int x, int y, int z);
		void setZstepSizeAndOrthoSize(float zStepSizeIn, float orthoSizeIn);
		void calcStepSizeXY();
		void polygonize(std::vector<bool>* vaVec);

		void createOriginGeometry(osg::Group* rootIn);

		inline osg::OpenMeshGeometry* getOpenMeshGeometry() { return m_geometry; };
	};
}//Namespace