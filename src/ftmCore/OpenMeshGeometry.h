//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OSG/Geometry>
#include "TriMesh_BindableAttribKernelT.h"


namespace osg {

	/* OpenMesh Geometry class.
	*  Provides binding of osg::Geometry to OpenMesh mesh */
	class OpenMeshGeometry : public Geometry
	{
	public:
		typedef Geometry Base;

		OpenMeshGeometry(osg::Vec4f selectedColorIn, osg::Vec4f notSelectedColorIn);
		~OpenMeshGeometry();

		/* Sets vertex array and binds it to internal OpenMesh mesh
		* \param array the vertex array, should be osg::Vec3Array */
		void setVertexArray(Array* array);

		//experimental added
		void setNormalArray(Array* array);

		/* Sets color array and binds it to internal OpenMesh mesh
		* \param array the color array, should be osg::Vec4Array */
		void setColorArray(Array* array);

		/* Sets texture array and binds it to internal OpenMesh mesh
		* \param unit texture unit using this texture coordinates array
		* \param array the texture coordinates array, should be osg::Vec2Array */
		void setTexCoordArray(unsigned int unit, Array* array);

		/* Add primitive set.
		*  Only one primitive set per geometry is supported
		* \param primitiveset the PrimitiveSet to be added, should be
		osg::DrawElementsUInt with TRIANGLE primitive type */
		bool addPrimitiveSet(PrimitiveSet* primitiveset);
		
		void flipNormals();
		void decimate();
		void writeFileToDisk(std::string safePath);

		osg::Vec3d getBarycentrumOfSelVerts();

		void scaleEntireGeometry(float valueIn);

		void grabVerticesStart(osg::Vec3f grabStartPositionIn);
		void grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, int snapToAxisIn, osg::Matrix matrixWorldToLocal);

		void grabPropEditStart(osg::Vec3f grabStartPositionIn, float radiusIn, float shapeIn);
		void grabPropEdit(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, int snapToAxisIn, osg::Matrix matrixWorldToLocal);
		void grabProbEditSetValues(float radiusIn, float shapeIn);

		void scaleVerticesStart();
		void scaleVertices(float scaleValueIn, bool isLocalIn, int axisIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn);

		void rotateVerticesStart();
		void rotateVertices(osg::Matrixd rotMatIn);

		void deleteSelectedVertices();

		//void next_halfedge_handle(int i);

		void extrude();

		void calcNormals();

		void sync_indices(size_t face, size_t v1, size_t v2, size_t v3);

		void copyAndStoreVertexArray();

		void cancelAnyTransformation();

		void translateSelColorToOMStatusBit();
		void translateSelOMStatusBitToColor();

		float calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh);

		void restoreLastTopology();

		void removeDoubles();

		void removeSeparatedObjects();

		void selectAllConnectedVertices(OpenMesh::VertexHandle vIt, std::vector<OpenMesh::VertexHandle>* selectedVertsInnerVector);

		//inline OpenMesh::OSG_TriMesh_BindableArrayKernelT getMesh(){ return mesh; };
		inline OpenMesh::OSG_TriMesh_BindableArrayKernelT* getMesh(){ return &mesh; };


	protected:
		typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;

		Mesh mesh;


	private:
		Vec3f m_grabStartPosition;
		Vec3Array* m_initialTopologyAtStartOfTransform;
		osg::Vec4f m_selectedColor, m_notSelectedColor;

		std::vector<bool> m_selectedPropEditVerts;
		std::vector<float> m_distanceFactorsForPropEdit;
		
		//EXP: arrays for historyManager
		std::vector<osg::Vec3Array*> m_topologyHistory;
	};

}