//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OpenMesh/Core/Utils/Property.hh>
#include "OSG_Vector_traits.h"

//*************************************************************
// OpenMesh property specializations for OSG types
//*************************************************************

namespace OpenMesh {

	template <>
	class PropertyT<osg::Vec2> : public BaseProperty
	{
	public:

		typedef osg::Vec2 T;
		typedef vector_traits<T>::container_type  container_type;
		typedef vector_traits<T>::vector_type		vector_type;
		typedef vector_traits<T>::value_type		value_type;
		typedef vector_type&						reference;
		typedef const vector_type&				const_reference;

	public:

		/// Default constructor
		PropertyT(const std::string& _name = "<unknown>")
			: BaseProperty(_name), data_(0)
		{
			data_ = new container_type();
		}

		/// Copy constructor
		PropertyT(const PropertyT & _rhs)
			: BaseProperty(_rhs), data_(_rhs.data_) {}

		// TODO: add safety measures around pointer handling
		void bind(container_type* _external_data)
		{
			data_ = _external_data;
		}

	public: // inherited from BaseProperty

		//virtual void free_mem()         { /*TODO:fix this*/ }
		virtual void clear()			  { (*data_).clear(); }
		virtual void reserve(size_t _n) { (*data_).reserve(_n); }
		virtual void resize(size_t _n)  { (*data_).resize(_n); }
		virtual void push_back()        { (*data_).push_back(vector_type()); }
		virtual void copy(size_t _io, size_t _i1) {}
		virtual void swap(size_t _i0, size_t _i1)
		{
			std::swap((*data_)[_i0], (*data_)[_i1]);
		}

	public:

		virtual void set_persistent(bool _yn)
		{
			check_and_set_persistent<T>(_yn);
		}

		virtual size_t       n_elements()   const { return (*data_).size(); }
		virtual size_t       element_size() const { return IO::size_of<T>(); }

#ifndef DOXY_IGNORE_THIS
		struct plus {
			size_t operator () (size_t _b, const T& _v)
			{
				return _b + IO::size_of<T>(_v);
			}
		};
#endif

		virtual size_t size_of(void) const
		{
			if (element_size() != IO::UnknownSize)
				return this->BaseProperty::size_of(n_elements());
			return std::accumulate((*data_).begin(), (*data_).end(), 0, plus());
		}

		virtual size_t size_of(size_t _n_elem) const
		{
			return this->BaseProperty::size_of(_n_elem);
		}

		virtual size_t store(std::ostream& _ostr, bool _swap) const
		{
			if (IO::is_streamable<vector_type>())
				return IO::store(_ostr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::store(_ostr, (*data_)[i], _swap);
			return bytes;
		}

		virtual size_t restore(std::istream& _istr, bool _swap)
		{
			if (IO::is_streamable<vector_type>())
				return IO::restore(_istr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::restore(_istr, (*data_)[i], _swap);
			return bytes;
		}

	public: // data access interface

		/// Get pointer to array (does not work for T==bool)
		const T* data() const {

			if ((*data_).empty())
				return 0;

			return &(*data_)[0];
		}

		/// Access the i'th element. No range check is performed!
		reference operator[](int _idx)
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Const access to the i'th element. No range check is performed!
		const_reference operator[](int _idx) const
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Make a copy of self.
		PropertyT<T>* clone() const
		{
			PropertyT<T>* p = new PropertyT<T>(*this);
			return NULL;
		}


	private:

		container_type *data_;
	};

	template <>
	class PropertyT<osg::Vec3> : public BaseProperty
	{
	public:

		typedef osg::Vec3							T;
		typedef vector_traits<T>::container_type  container_type;
		typedef vector_traits<T>::vector_type		vector_type;
		typedef vector_traits<T>::value_type		value_type;
		typedef vector_type&						reference;
		typedef const vector_type&				const_reference;

	public:

		/// Default constructor
		PropertyT(const std::string& _name = "<unknown>")
			: BaseProperty(_name), data_(0)
		{
			data_ = new container_type();
		}

		/// Copy constructor
		PropertyT(const PropertyT & _rhs)
			: BaseProperty(_rhs), data_(_rhs.data_) {}

		// TODO: add safety measures around pointer handling
		void bind(container_type* _external_data)
		{
			data_ = _external_data;
		}

	public: // inherited from BaseProperty

		//virtual void free_mem()         { /*TODO:fix this*/ }
		virtual void clear()			  { (*data_).clear(); }
		virtual void reserve(size_t _n) { (*data_).reserve(_n); }
		virtual void resize(size_t _n)  { (*data_).resize(_n); }
		virtual void push_back()        { (*data_).push_back(vector_type()); }
		//PL: copy was not implemented
		virtual void copy(size_t _io, size_t _i1)
		{
			(*data_)[_i1] = (*data_)[_io];
		}
		virtual void swap(size_t _i0, size_t _i1)
		{
			std::swap((*data_)[_i0], (*data_)[_i1]);
		}

	public:

		virtual void set_persistent(bool _yn)
		{
			check_and_set_persistent<T>(_yn);
		}

		virtual size_t       n_elements()   const { return (*data_).size(); }
		virtual size_t       element_size() const { return IO::size_of<T>(); }

#ifndef DOXY_IGNORE_THIS
		struct plus {
			size_t operator () (size_t _b, const T& _v)
			{
				return _b + IO::size_of<T>(_v);
			}
		};
#endif

		virtual size_t size_of(void) const
		{
			if (element_size() != IO::UnknownSize)
				return this->BaseProperty::size_of(n_elements());
			return std::accumulate((*data_).begin(), (*data_).end(), 0, plus());
		}

		virtual size_t size_of(size_t _n_elem) const
		{
			return this->BaseProperty::size_of(_n_elem);
		}

		virtual size_t store(std::ostream& _ostr, bool _swap) const
		{
			if (IO::is_streamable<vector_type>())
				return IO::store(_ostr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::store(_ostr, (*data_)[i], _swap);
			return bytes;
		}

		virtual size_t restore(std::istream& _istr, bool _swap)
		{
			if (IO::is_streamable<vector_type>())
				return IO::restore(_istr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::restore(_istr, (*data_)[i], _swap);
			return bytes;
		}

	public: // data access interface

		/// Get pointer to array (does not work for T==bool)
		const T* data() const {

			if ((*data_).empty())
				return 0;

			return &(*data_)[0];
		}

		/// Access the i'th element. No range check is performed!
		reference operator[](int _idx)
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Const access to the i'th element. No range check is performed!
		const_reference operator[](int _idx) const
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Make a copy of self.
		PropertyT<T>* clone() const
		{
			//PropertyT<T>* p = new PropertyT<T>( *this );
			return NULL;
		}


	private:

		container_type *data_;
	};


	template <>
	class PropertyT<osg::Vec4> : public BaseProperty
	{
	public:

		typedef osg::Vec4 T;
		typedef vector_traits<T>::container_type  container_type;
		typedef vector_traits<T>::vector_type		vector_type;
		typedef vector_traits<T>::value_type		value_type;
		typedef vector_type&						reference;
		typedef const vector_type&				const_reference;

	public:

		/// Default constructor
		PropertyT(const std::string& _name = "<unknown>")
			: BaseProperty(_name), data_(0)
		{
			data_ = new container_type();
		}

		/// Copy constructor
		PropertyT(const PropertyT & _rhs)
			: BaseProperty(_rhs), data_(_rhs.data_) {}

		// TODO: add safety measures around pointer handling
		void bind(container_type* _external_data)
		{
			data_ = _external_data;
		}

	public: // inherited from BaseProperty

		//virtual void free_mem()         { /*TODO:fix this*/ }
		virtual void clear()			  { (*data_).clear(); }
		virtual void reserve(size_t _n) { (*data_).reserve(_n); }
		virtual void resize(size_t _n)  { (*data_).resize(_n); }
		virtual void push_back()        { (*data_).push_back(vector_type()); }
		virtual void copy(size_t _io, size_t _i1) {}
		virtual void swap(size_t _i0, size_t _i1)
		{
			std::swap((*data_)[_i0], (*data_)[_i1]);
		}

	public:

		virtual void set_persistent(bool _yn)
		{
			check_and_set_persistent<T>(_yn);
		}

		virtual size_t       n_elements()   const { return (*data_).size(); }
		virtual size_t       element_size() const { return IO::size_of<T>(); }

#ifndef DOXY_IGNORE_THIS
		struct plus {
			size_t operator () (size_t _b, const T& _v)
			{
				return _b + IO::size_of<T>(_v);
			}
		};
#endif

		virtual size_t size_of(void) const
		{
			if (element_size() != IO::UnknownSize)
				return this->BaseProperty::size_of(n_elements());
			return std::accumulate((*data_).begin(), (*data_).end(), 0, plus());
		}

		virtual size_t size_of(size_t _n_elem) const
		{
			return this->BaseProperty::size_of(_n_elem);
		}

		virtual size_t store(std::ostream& _ostr, bool _swap) const
		{
			if (IO::is_streamable<vector_type>())
				return IO::store(_ostr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::store(_ostr, (*data_)[i], _swap);
			return bytes;
		}

		virtual size_t restore(std::istream& _istr, bool _swap)
		{
			if (IO::is_streamable<vector_type>())
				return IO::restore(_istr, (*data_), _swap);
			size_t bytes = 0;
			for (size_t i = 0; i < n_elements(); ++i)
				bytes += IO::restore(_istr, (*data_)[i], _swap);
			return bytes;
		}

	public: // data access interface

		/// Get pointer to array (does not work for T==bool)
		const T* data() const {

			if ((*data_).empty())
				return 0;

			return &(*data_)[0];
		}

		/// Access the i'th element. No range check is performed!
		reference operator[](int _idx)
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Const access to the i'th element. No range check is performed!
		const_reference operator[](int _idx) const
		{
			assert(size_t(_idx) < (*data_).size());
			return (*data_)[_idx];
		}

		/// Make a copy of self.
		PropertyT<T>* clone() const
		{
			//PropertyT<T>* p = new PropertyT<T>( *this );
			return NULL;
		}


	private:

		container_type *data_;
	};

	//***********************************************
	//	Specialization for DrawElements indices
	//***********************************************

	template <>
	class PropertyT<GLuint> : public BaseProperty
	{
	public:

		typedef GLuint T;
		typedef vector_traits<T>::container_type  container_type;
		typedef vector_traits<T>::vector_type		vector_type;
		typedef vector_traits<T>::value_type	value_type;
		typedef vector_type&						reference;
		typedef const vector_type&				const_reference;

	public:

		/// Default constructor
		PropertyT(const std::string& _name = "<unknown>")
			: BaseProperty(_name), data_(0)
		{
			data_ = new container_type();
		}

		/// Copy constructor
		PropertyT(const PropertyT & _rhs)
			: BaseProperty(_rhs), data_(_rhs.data_) {}

		// TODO: add safety measures around pointer handling
		void bind(container_type* _external_data)
		{
			data_ = _external_data;
		}

	public: // inherited from BaseProperty

		//virtual void free_mem()         { /*TODO:fix this*/ }
		virtual void clear()			  { (*data_).clear(); }

		virtual void reserve(size_t _n)
		{
			(*data_).reserve(3 * _n);
		}

		virtual void resize(size_t _n)
		{
			(*data_).resize(3 * _n);
		}

		virtual void push_back()
		{
			(*data_).push_back(0);
			(*data_).push_back(0);
			(*data_).push_back(0);
		}

		virtual void swap(size_t _i0, size_t _i1)
		{
			std::swap((*data_)[3 * _i0], (*data_)[3 * _i1]);
			std::swap((*data_)[3 * _i0 + 1], (*data_)[3 * _i1 + 1]);
			std::swap((*data_)[3 * _i0 + 2], (*data_)[3 * _i1 + 2]);
		}

		virtual void copy(size_t _io, size_t _i1) {}
	public:

		virtual void set_persistent(bool _yn)
		{
			throw std::exception("not implemented!");
		}

		virtual size_t       n_elements()   const { return (*data_).size() / 3; }
		virtual size_t       element_size() const { return IO::size_of<T>(); }

#ifndef DOXY_IGNORE_THIS
		struct plus {
			size_t operator () (size_t _b, const T& _v)
			{
				return _b + IO::size_of<T>(_v);
			}
		};
#endif

		virtual size_t size_of(void) const
		{
			throw std::exception("not implemented!");
		}

		virtual size_t size_of(size_t _n_elem) const
		{
			throw std::exception("not implemented!");
		}

		virtual size_t store(std::ostream& _ostr, bool _swap) const
		{
			throw std::exception("not implemented!");
		}

		virtual size_t restore(std::istream& _istr, bool _swap)
		{
			throw std::exception("not implemented!");
		}

	public: // data access interface

		/// Get pointer to array (does not work for T==bool)
		const T* data() const
		{
			throw std::exception("not implemented!");
		}

		/// Access the i'th element. No range check is performed!
		reference operator[](int _idx)
		{
			return (*data_)[_idx];
		}

		/// Const access to the i'th element. No range check is performed!
		const_reference operator[](int _idx) const
		{
			return (*data_)[_idx];
		}

		/// Make a copy of self.
		PropertyT<T>* clone() const
		{
			//PropertyT<T>* p = new PropertyT<T>( *this );
			return NULL;
		}


	private:

		container_type *data_;
	};

}