//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#include "OpenMeshGeometry.h"
#include "OSG_PropertyT.h" 
#include <OpenMesh/Core/Mesh/TriMeshT.hh>
#include <OpenMesh/Core/IO/MeshIO.hh>

#include <iostream>
#include <fstream>
#include <stdlib.h>


//#include "osg/Texture2D"
//#include "osgDB\ReadFile"

using namespace osg;

OpenMeshGeometry::OpenMeshGeometry(osg::Vec4f selectedColorIn, osg::Vec4f notSelectedColorIn) : Geometry()
{
	setUseDisplayList(false);
	setUseVertexBufferObjects(true);
	setDataVariance(DYNAMIC);

	osg::Vec3f m_grabStartPosition = osg::Vec3f();
	//EXP: Used for restore the original topology of the mesh
	m_initialTopologyAtStartOfTransform = new Vec3Array();

	m_selectedColor = osg::Vec4f();
	m_notSelectedColor = osg::Vec4f();
	m_selectedColor = selectedColorIn;
	m_notSelectedColor = notSelectedColorIn;

	mesh.request_face_normals();
	mesh.request_vertex_normals();

}

OpenMeshGeometry::~OpenMeshGeometry()
{
}

void OpenMeshGeometry::setVertexArray(Array* array)
{
	Base::setVertexArray(array);
	VertexBufferObject *vbo = getOrCreateVertexBufferObject();
	//EXP: STREAM - STATIC - DYNAMIC as options ->Look OpenGL API (Maybe DYNAMIC will be a better choice)
	vbo->setUsage(GL_STREAM_DRAW);


	//mesh.request_vertex_normals();
	//Vec3Array *normalArray = new Vec3Array();
	//this->setNormalArray(normalArray);
	//this->setNormalBinding(osg::OpenMeshGeometry::BIND_PER_PRIMITIVE_SET);
	//mesh.bind<Vec3>(Mesh::VProp,"v:normals",*normalArray);


	Vec3Array *vertexArray = dynamic_cast<Vec3Array*>(array);
	if (vertexArray)
	{
		mesh.resize(vertexArray->size(), 0, 0);
		mesh.bind<Vec3>(Mesh::VProp, "v:points", *vertexArray);
	}

}

void OpenMeshGeometry::setTexCoordArray(unsigned int unit, Array* array)
{
	Base::setTexCoordArray(unit, array);
	Vec2Array *texCoordArray = dynamic_cast<Vec2Array*>(array);
	if (texCoordArray)
	{
		mesh.request_vertex_texcoords2D();
		mesh.bind<Vec2>(Mesh::VProp, "v:texcoords2D", *texCoordArray);
	}
}

void OpenMeshGeometry::setColorArray(Array* array)
{
	Base::setColorArray(array);
	Vec4Array *colorArray = dynamic_cast<Vec4Array*>(array);
	if (colorArray)
	{
		mesh.request_vertex_colors();
		mesh.bind<Vec4>(Mesh::VProp, "v:colors", *colorArray);
	}
}

//void OpenMeshGeometry::setNormalArray(Array* array)
//{
//See addPrimitiveSet(PrimitiveSet* primitiveSet)
//}

bool OpenMeshGeometry::addPrimitiveSet(PrimitiveSet* primitiveSet)
{
	if (primitiveSet->getType() != PrimitiveSet::DrawElementsUIntPrimitiveType)
	{
		throw std::exception("Primitive type not supported!");
	}

	if (Base::addPrimitiveSet(primitiveSet))
	{
		//PL: Error prone??!
		DrawElementsUInt* indices = dynamic_cast<DrawElementsUInt*>(primitiveSet);

		std::cout << " Es gibt in diesem Objekt soviele Indicies:" << indices->getNumIndices() << std::endl;
		for (unsigned int i = 0; i < indices->getNumIndices(); ++i)
		{
			OpenMesh::VertexHandle v1((*indices)[i]);
			++i;
			OpenMesh::VertexHandle v2((*indices)[i]);
			++i;
			OpenMesh::VertexHandle v3((*indices)[i]);
			mesh.add_face(v1, v2, v3);
		}
		mesh.bind<GLuint>(Mesh::FProp, "indices", *indices);


		osg::ref_ptr<Vec3Array> normalArray = new Vec3Array();
		Base::setNormalArray(normalArray);
		Base::setNormalBinding(osg::OpenMeshGeometry::BIND_PER_VERTEX);
		mesh.bind<Vec3>(Mesh::VProp, "v:normals", *normalArray);
		mesh.garbage_collection(true, true, true);
		mesh.update_normals();

		return true;
	}
	return false;
}

osg::Vec3d OpenMeshGeometry::getBarycentrumOfSelVerts()
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	int i = 0;
	int sumOfSelectedVertices = 0;
	double xSum = 0;
	double ySum = 0;
	double zSum = 0;

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//if (selectedVerticesIn.at(i))
		if (mesh.status(*vIt).selected())
		{
			xSum += mesh.point(vIt.handle()).x();
			ySum += mesh.point(vIt.handle()).y();
			zSum += mesh.point(vIt.handle()).z();
			sumOfSelectedVertices++;
		}
		i++;
	}
	xSum = xSum / sumOfSelectedVertices;
	ySum = ySum / sumOfSelectedVertices;
	zSum = zSum / sumOfSelectedVertices;

	return osg::Vec3d(xSum, ySum, zSum);
}

void OpenMeshGeometry::flipNormals()
{
	Mesh::FaceIter  fIt, fBegin, fEnd;
	fBegin = mesh.faces_begin();
	fEnd = mesh.faces_end();
	int i = 0;
	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		Mesh::FaceVertexIter fvIter = mesh.fv_iter(fIt);
		for (; fvIter; ++fvIter)
		{
			mesh.set_normal(fvIter, -mesh.calc_face_normal(fIt));
		}
	}
}

void OpenMeshGeometry::writeFileToDisk(std::string safePath)
{
	std::cout << "Start exporting mesh with OpenMesh" << std::endl;
	OpenMesh::IO::Options opt;

	opt += OpenMesh::IO::Options::Binary;
	if (!OpenMesh::IO::write_mesh(mesh, safePath, opt))
	{
		std::cerr << "write error\n";
		exit(1);
	}
	std::cout << "Exproting finished" << std::endl;
}

void OpenMeshGeometry::removeDoubles()
{
	Mesh::VertexIter vItInner, vItOutter, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	unsigned int counter = 0;
	for (vItOutter = vBegin; vItOutter != vEnd; ++vItOutter)
	{
		std::cout << "A" << std::endl;
		//std::cout << "inner: " << vItInner.handle().idx() << std::endl;
		//std::cout << "outer: " << vItOutter.handle().idx() << std::endl;
		for (vItInner = vItOutter; vItInner != vEnd;)
		{
			//std::cout << "B" << std::endl;
			++vItInner;
			//std::cout << "innerinner: " << vItInner.handle().idx() << std::endl;

			//Ich habe hier den Abstand zwei Vertices. Unterstreiten die Verticies einen bestimmten Abstand, dann sind sie nah beieinander und m�sen gemerged werden
			osg::Vec3f distanceVector = mesh.point(*vItOutter) - mesh.point(*vItInner);
			float distance = distanceVector.length();
			//std::cout << "distance: " << distance << std::endl;

			if (distance < 0.00000002)
			{
				//std::cout << "Vec: " << mesh.point(vItOutter).x() << " " << mesh.point(vItOutter).y() << " " << mesh.point(vItOutter).z() << std::endl;
				//std::cout << "Vec: " << mesh.point(vItInner).x() << " " << mesh.point(vItInner).y() << " " << mesh.point(vItInner).z() << std::endl;
				//std::cout << "______ " << std::endl;

				int counterVertices = 0;
				int counterFaces = 0;
				std::vector<OpenMesh::VertexHandle> vertsToAdd;
				Mesh::VertexFaceIter vfIt;

				//Ich schaue im Umfeld vom Vertex vItOutter alle Faces an ***
				for (vfIt = mesh.vf_iter(*vItOutter); vfIt.is_valid(); ++vfIt)
				{
					std::cout << "C" << std::endl;
					//Does it always only contain one face? No!
					counterFaces++;
					//Hier habe ich eine Auswahl an Faces, die ich im Endeffekt l�schen muss
					Mesh::FaceVertexIter fvIt;
					for (fvIt = mesh.fv_iter(*vfIt); fvIt.is_valid(); ++fvIt)
					{
						//Wenn ich hier rein komme, dann iteriere ich einmal �ber alle Verts auf dem jeweiligen Face und f�lle ein Array. Und immer wenn das OuterVert gleich dem hinzuzuf�genden Vert ist, dann muss ich ein neues gemeinsamen Verts hinzuf�gen. Also das
						//std::cout << "Vec: " << mesh.point(vItInner).x() << " " << mesh.point(vItInner).y() << " " << mesh.point(vItInner).z() << std::endl;
						//if (mesh.point(vItOutter) != mesh.point(fvIt))
						//TODO: speedup with idx?
						if (mesh.point(*vItOutter) == mesh.point(*fvIt))
						{
							vertsToAdd.push_back(*vItInner);
							std::cout << "In if" << std::endl;
						}
						else
						{
							//std::cout << "Vec: " << mesh.point(vItOutter).x() << " " << mesh.point(vItOutter).y() << " " << mesh.point(vItOutter).z() << std::endl;
							//std::cout << "Vec: " << mesh.point(fvIt).x() << " " << mesh.point(fvIt).y() << " " << mesh.point(fvIt).z() << std::endl;
							std::cout << "In else" << std::endl;
							vertsToAdd.push_back(*fvIt);
							counterVertices++;

						}
					}

				}
				std::cout << "Ehm? " << counterVertices << std::endl;

				for (int newTriangles = 0; newTriangles < vertsToAdd.size() / 3; newTriangles++)
				{
					mesh.add_face(vertsToAdd.at(newTriangles * 3), vertsToAdd.at(newTriangles * 3 + 1), vertsToAdd.at(newTriangles * 3 + 2));
				}


				std::cout << "Verts: " << counterVertices << std::endl;
				//std::cout << "Faces: " << counterFaces << std::endl;
				//mesh.delete_vertex(vItOutter);
				counter++;
			}

			//mesh.set_point(vIt.handle(), mesh.point(vIt.handle()) * valueIn);
		}
	}
	//HWM: Hier alles zusammenbauen ############################################################################################
	std::cout << "kurz vorher" << std::endl;
	mesh.delete_vertex(*vItOutter, true);
	std::cout << "kurz danach" << std::endl;
	mesh.garbage_collection(false, true, true);

	//std::cout << "counter:" << counter << std::endl;
	mesh.garbage_collection();
	getVertexArray()->dirty();
	//std::cout << "doubles:" << count << std::endl;

	std::cout << "nach GC" << std::endl;
	if (!OpenMesh::IO::write_mesh(mesh, "data/monkey-processed.obj"))
	{
		std::cerr << "write error\n";
		exit(1);
	}
}

void OpenMeshGeometry::decimate()
{
	//#######################################################################Hier weiter machen
}

void OpenMeshGeometry::scaleEntireGeometry(float valueIn)
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		mesh.set_point(vIt.handle(), mesh.point(vIt.handle()) * valueIn);
	}
	getVertexArray()->dirty();

}

void OpenMeshGeometry::grabVerticesStart(osg::Vec3f grabStartPositionIn)
{
	m_grabStartPosition = grabStartPositionIn;
	//EXP: Store original geometry
	copyAndStoreVertexArray();
}


//NOTE: Good Function to test runtimes of different solutions with 126000verts monkey in debug:
void OpenMeshGeometry::grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, int snapToAxisIn, osg::Matrix matrixWorldToLocal)
{
	osg::Vec3f deltaPosition = newPositionIn - m_grabStartPosition;
	if (!isLocalCoordSysSelectedIn)
	{
		if (snapToAxisIn == 1)
		{
			deltaPosition.y() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (snapToAxisIn == 2)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (snapToAxisIn == 3)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.y() = 0.0f;
		}
	}
	//EXP: Get the rotation of the geometry, to translate deltaPosition from global to local space

	deltaPosition = matrixWorldToLocal.getRotate() * deltaPosition;

	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getVertexArray());

	int i = 0;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			if (isLocalCoordSysSelectedIn)
			{
				if (snapToAxisIn == 1)
				{
					deltaPosition.y() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (snapToAxisIn == 2)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (snapToAxisIn == 3)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.y() = 0.0f;
				}
			}
			//NOTE: The old way of transforming in OSGScene (debug - 126000verts monkey - select all - grab it) -> 36 - 38fps!!! new way about 25fps...


			//osg::Vec3f  delta = m_initialTopologyAtStartOfTransform->at(i) + deltaPosition;
			//NOTE: Without delta 24.8fps, with delta ~24.7fps
			vertexArray->at(i) = m_initialTopologyAtStartOfTransform->at(i) + deltaPosition;;

			//NOTE: 17fps -> no difference between vIt.handle(), vIt und *vIt
			//mesh.set_point(*vIt,m_initialTopologyAtStartOfTransform->at(i) + deltaPosition);
		}
		i++;
	}
	vertexArray->dirty();
}

void OpenMeshGeometry::grabPropEditStart(osg::Vec3f grabStartPositionIn, float radiusIn, float shapeIn)
{
	m_grabStartPosition = grabStartPositionIn;
	//EXP: Store original geometry
	copyAndStoreVertexArray();

	m_selectedPropEditVerts.resize(mesh.n_vertices());
	m_distanceFactorsForPropEdit.resize(mesh.n_vertices());


	Mesh::VertexHandle vSelected;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	//ToDo: What to do when multiple points are selected? Barycentrum?
	//Check which point is selected and store this as a vertexHandle
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			vSelected = *vIt;
		}
	}

	//Determine distanceFactors - this factor is the value between 0 and 1 which indicates how far away is a point of the mesh to the single selected point
	//When value is near by 1 - the related point will be more transformed than a value near by 0
	if (vSelected.is_valid())
	{
		int i = 0;
		for (vIt = vBegin; vIt != vEnd; ++vIt, i++)
		{
			m_selectedPropEditVerts.at(i) = false;
			if (mesh.status(*vIt).selected())
			{
				m_selectedPropEditVerts.at(i) = true;
				m_distanceFactorsForPropEdit.at(i) = 1.0f;
			}
			else
			{
				float distance = calculateDistance(vIt, vSelected);
				if (distance <= radiusIn)
				{
					m_selectedPropEditVerts.at(i) = true;
					m_distanceFactorsForPropEdit.at(i) = 1 - ((pow(distance, shapeIn) / pow(radiusIn, shapeIn)));
				}
			}
		}
	}
}

void OpenMeshGeometry::grabPropEdit(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, int snapToAxisIn, osg::Matrix matrixWorldToLocal)
{
	osg::Vec3f deltaPosition = newPositionIn - m_grabStartPosition;
	if (!isLocalCoordSysSelectedIn)
	{
		if (snapToAxisIn == 1)
		{
			deltaPosition.y() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (snapToAxisIn == 2)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (snapToAxisIn == 3)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.y() = 0.0f;
		}
	}
	//EXP: Get the rotation of the geometry, to translate deltaPosition from global to local space
	deltaPosition = matrixWorldToLocal.getRotate() * deltaPosition;

	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getVertexArray());

	int i = 0;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_selectedPropEditVerts.at(i))
		{
			if (isLocalCoordSysSelectedIn)
			{
				if (snapToAxisIn == 1)
				{
					deltaPosition.y() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (snapToAxisIn == 2)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (snapToAxisIn == 3)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.y() = 0.0f;
				}

				vertexArray->at(i) = m_initialTopologyAtStartOfTransform->at(i) + deltaPosition * m_distanceFactorsForPropEdit.at(i);
			}
		}
		i++;
	}
	vertexArray->dirty();
}

void OpenMeshGeometry::grabProbEditSetValues(float radiusIn, float shapeIn)
{
	m_selectedPropEditVerts.resize(mesh.n_vertices());
	m_distanceFactorsForPropEdit.resize(mesh.n_vertices());

	Mesh::VertexHandle vSelected;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	//Check which point is selected and store this as a vertexHandle
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			vSelected = *vIt;
		}
	}

	if (vSelected.is_valid())
	{
		int i = 0;
		for (vIt = vBegin; vIt != vEnd; ++vIt, i++)
		{
			m_selectedPropEditVerts.at(i) = false;
			if (mesh.status(*vIt).selected())
			{
				m_selectedPropEditVerts.at(i) = true;
				m_distanceFactorsForPropEdit.at(i) = 1.0f;
			}
			else
			{
				float distance = calculateDistance(vIt, vSelected);
				if (distance <= radiusIn)
				{
					m_selectedPropEditVerts.at(i) = true;
					m_distanceFactorsForPropEdit.at(i) = 1 - ((pow(distance, shapeIn) / pow(radiusIn, shapeIn)));
				}
			}
		}
	}
}

void OpenMeshGeometry::scaleVerticesStart()
{
	//Store original geometry
	copyAndStoreVertexArray();
}

//TODO: I get relative values from scaleValueIn, like -0.064. Relative is bad, because we cant determine how far we have already scaled the geometry.
//		When we get absolute values, we can calculate every time the new position of a vertex and for this we can use the originalTopology.
//		When we use the originalTopology, we can set it easily back, when we cancel the transformation. But this is not the real benefit:
//		Everytime when we switch the axes, we can restore the originalTopology easily.... but , to be honest, this is almost the same application like this before
//		Anyway, I will move the grab functionality in this class. I think I need an originalTopology as VertexArray or the entiry Mesh from this class. Both as deep copy.
//		First I have to figure out how big is mesh and how big will be a vertexArray. Is the mesh much bigger? Maybe it is better to take a mesh anyway, 
//		because we can restore normals, UVCoords and all the stuff, if that is included in mesh...
//		When we take mesh, I have to figure out how to make a deep copy of the mesh object..
void OpenMeshGeometry::scaleVertices(float scaleValueIn, bool isLocalIn, int axisIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn)
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec3f barycentrum = getBarycentrumOfSelVerts();

	if (isLocalIn)
	{
		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			//if (selectedVerticesIn.at(i))
			if (mesh.status(*vIt).selected())
			{
				osg::Vec3d newPosition;
				osg::Vec3d vectorToBarycentrum;
				vectorToBarycentrum = barycentrum - mesh.point(vIt.handle());

				if (axisIn == 0)
				{
					newPosition = vectorToBarycentrum * scaleValueIn;
				}
				else if (axisIn == 1)
				{
					newPosition.x() = vectorToBarycentrum.x() * scaleValueIn;
					newPosition.y() = 0.0f;
					newPosition.z() = 0.0f;
				}
				else if (axisIn == 2)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = vectorToBarycentrum.y() * scaleValueIn;
					newPosition.z() = 0.0f;
				}
				else if (axisIn == 3)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = 0.0f;
					newPosition.z() = vectorToBarycentrum.z() * scaleValueIn;
				}
				//m_initialTopologyAtStartOfTransform
				mesh.set_point(vIt.handle(), mesh.point(vIt.handle()) + (newPosition));
			}
		}
	}
	//FIXME: global scaling dont work at this time - selection of global axes are disabled within the event "Hydra_Button_Joystick_Controller1"
	else
	{
		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			//if (selectedVerticesIn.at(i))
			if (mesh.status(*vIt).selected())
			{
				osg::Vec3d newPosition;
				osg::Vec3d vectorToBarycentrum;
				vectorToBarycentrum = barycentrum - mesh.point(vIt.handle());
				vectorToBarycentrum = vectorToBarycentrum * matrixLocalToWorldIn;
				if (axisIn == 0)
				{
					newPosition = vectorToBarycentrum * scaleValueIn;
				}
				else if (axisIn == 1)
				{
					newPosition.x() = vectorToBarycentrum.x() * scaleValueIn;
					newPosition.y() = 0.0f;
					newPosition.z() = 0.0f;
				}
				else if (axisIn == 2)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = vectorToBarycentrum.y() * scaleValueIn;
					newPosition.z() = 0.0f;
				}
				else if (axisIn == 3)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = 0.0f;
					newPosition.z() = vectorToBarycentrum.z() * scaleValueIn;
				}
				//newPosition = newPosition * matrixWorldToLocal;
				mesh.set_point(vIt.handle(), mesh.point(vIt.handle()) + (newPosition));
			}
		}
	}
	getVertexArray()->dirty();

}

void OpenMeshGeometry::rotateVerticesStart()
{
	copyAndStoreVertexArray();
}

void OpenMeshGeometry::rotateVertices(osg::Matrixd rotMatIn)
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec3f barycentrum = getBarycentrumOfSelVerts();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//if (selectedVerticesIn.at(i))
		if (mesh.status(*vIt).selected())
		{
			osg::Matrixd transMatrix;
			transMatrix.makeTranslate(barycentrum);

			osg::Matrixd transMatrixBack;
			transMatrixBack.makeTranslate(-barycentrum);

			mesh.set_point(vIt.handle(), mesh.point(vIt.handle()) * (transMatrixBack * rotMatIn * transMatrix));

			//FIXME: Update Normals? Nothing will run.... request_face_normals has no effect - "set_normal" and "update_face_normals" will be call an exception 
			//mesh.set_normal(vIt.handle(), mesh.normal(vIt.handle()) * (transMatrixBack * rotMat * transMatrix));
			//mesh.update_face_normals();
		}
	}
	getVertexArray()->dirty();
}

void OpenMeshGeometry::deleteSelectedVertices()
{
	//NotePL: First delete faces, than vertices - it will otherwise end in an assert in polyconnectivity --- assert(_fh.is_valid() && !status(_fh).deleted()); ---
	Mesh::FaceIter  fIt, fBegin, fEnd;
	Mesh::VertexIter vIt, vBegin, vEnd;
	int j = 0;

	//TODO: Need this?
	mesh.request_edge_status();
	mesh.request_halfedge_status();
	mesh.request_face_status();
	mesh.request_vertex_status();

	//Read is also available now
	//if (!OpenMesh::IO::read_mesh(mesh, "data/monkey.obj"))
	//{
	//	std::cerr << "read error\n";
	//	exit(1);
	//}

	int i;
	// Delete Faces #################################################
	//First always delete faces
	//fBegin = mesh.faces_begin();
	//fEnd = mesh.faces_end();
	//int i = 0;
	//for (fIt = fBegin; fIt != fEnd; ++fIt)
	//{
	//	if (i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20 || i == 21 || i == 22 ||
	//		i == 23 || i == 24 || i == 25 || i == 26 || i == 27 || i == 28 || i == 29 || i == 30 || i == 31 || i == 32)
	//	{
	//		//mesh.delete_face(fIt.handle(), false);
	//		//std::cout << mesh.status(fIt).deleted() << std::endl;
	//		//EXP: Maybe I have to bind the data.
	//		//Maybe the stuff is already binded and I only have to add some properties (I think, this is probably the way to go)
	//		//Have a look at BindableAttribKernerlT.h
	//		//mesh.bind<Vec3>(Mesh::VProp, "v:points", *vertexArray);
	//	}
	//	if (i == 946 ) //Monkey face in view i == 22
	//	{
	//		//mesh.delete_face(fIt.handle(), false);
	//		//std::cout << mesh.status(fIt).deleted() << std::endl;
	//		//EXP: Maybe I have to bind the data.
	//		//Maybe the stuff is already binded and I only have to add some properties (I think, this is probably the way to go)
	//		//Have a look at BindableAttribKernerlT.h
	//		//mesh.bind<Vec3>(Mesh::VProp, "v:points", *vertexArray);
	//		
	//	}
	//	i++;
	//}

	// Delete Vertices #################################################
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();
	i = 0;
	//mesh.request_vertex_status();
	//std::cout << "Has Vertex Status: " << mesh.has_vertex_status() << std::endl;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{

		//if (selectedVerticesIn.at(i))
		if (mesh.status(*vIt).selected())
		{

			//Mesh::VertexFaceIter vFIt, vFItBegin, vFItEnd;
			//vFItBegin = mesh.vf_begin(vIt);
			//vFItEnd = mesh.vf_end(vIt);
			//for (vFIt = vFItBegin; vFIt != vFItEnd; ++vFIt)
			//{
			//	//mesh.delete_face(vFIt.handle(), false);
			//	deleteVector.push_back(vFIt.handle().idx());
			//	std::cout << "drin" << std::endl;
			//}

			//for (Mesh::VertexVertexIter vv_it = mesh.vv_iter(*vIt); vv_it.is_valid(); ++vv_it)
			//{
			//	std::cout << "do something with e.g.mesh.point(*vv_it) " << std::endl;
			//	//mesh.delete_vertex(*vv_it, false);
			//}

			mesh.delete_vertex(vIt.handle(), true);
		}
	}

	//
	mesh.garbage_collection(true, true, true);

	//std::cout << "nach GC" << std::endl;
	//myfile << "nach GC" << std::endl;

	//if (!OpenMesh::IO::write_mesh(mesh, "data/monkey-processed.obj"))
	//{
	//	std::cerr << "write error\n";
	//	exit(1);
	//}


	//NOTE: Important: Clean up indicies
	size_t nF;
	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (i = 0, nF = mesh.n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = mesh.cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	//FIXME: After deleting there are some normals pointing in the wrong direction
	// I think, sync_indicies() mixed up some verts and when we calculate normals, there will be wrong normals
	//The solution is something like in blender "calculate normals outside"
	//Other solution will be that every normals pointing in and out - a "double normal" - maybe good for modeling, you dont want to see black faces...
	//the following lines dont help:
	mesh.update_normals();
	//getNormalArray()->dirty();

	////TODO: Assign the export functionality of OpenMesh in the Menu
	//if (!OpenMesh::IO::write_mesh(mesh, "monkey-processed-fromOBJ.ply"))
	//{
	//	std::cerr << "write error\n";
	//	exit(1);
	//}
}

void OpenMeshGeometry::extrude()
{
	//FIXME: Abfrage ob �berhaupt irgendwas ausgew�hlt ist um errors zu vermeiden?

	std::map<Mesh::VertexHandle, Mesh::VertexHandle> extrudeVertexMap;
	std::vector<std::vector<Mesh::VertexHandle> > extrudedQuads;
	std::vector<Mesh::VertexHandle> alreadyProcessedVerts;

	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			extrudeVertexMap[*vIt] = mesh.add_vertex(mesh.point(*vIt) * 1.001f);
		}
	}

	//Go again over all verts in mesh...
	unsigned int numExtrudedQuads = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//...and if it is selected...
		if (mesh.status(*vIt).selected())
		{
			//...then create an vertexIter and look around on each neighboor verts....
			for (Mesh::VertexVertexIter vvIter = mesh.vv_iter(*vIt); vvIter; ++vvIter)
			{
				bool isAlreadyProcessed = false;
				//Das macht keinen sinn...
				//Wenn ich hier rein gehe, ist alreadyProcessedVerts immer 0!
				//Das bedeutet er geht einmal durch
				//Aber unten steht ja noch mehr und das ist eine for schleife
				for (unsigned int ente = 0; ente < alreadyProcessedVerts.size(); ente++)
				{
					if (*vvIter == alreadyProcessedVerts.at(ente))
					{
						isAlreadyProcessed = true;
					}
				}

				//Ich mach hier mit mal weiter: 
				//Wenn der mesh status selectiert ist und dieser Vertex noch nicht verarbeitet wurden ist..
				//Also das bedeutet, das ist der nachbar vertex, welcher auch selektiert aber nicht extrudiert ist
				if (mesh.status(*vvIter).selected() && !isAlreadyProcessed)
				{
					//Wenn er selektiert ist und noch nicht verarbeitet, dann
					//f�lle nun das QuadSpeicherfeld-> Hier wird frischer Vector mit VertexHandlen gepusht
					extrudedQuads.push_back(std::vector<Mesh::VertexHandle>());
					//Und es wird nach und nach der selektiert Vert..
					extrudedQuads.at(numExtrudedQuads).push_back(*vIt);
					//..der zugeh�rige extrudierte Vert...
					extrudedQuads.at(numExtrudedQuads).push_back(extrudeVertexMap[*vIt]);
					//...der erkannte, selektierte (aber noch nicht verarbeitete) NachbarVert...
					extrudedQuads.at(numExtrudedQuads).push_back(*vvIter);
					//..mit seinem extrudiertem Vert
					extrudedQuads.at(numExtrudedQuads).push_back(extrudeVertexMap[*vvIter]);
					//Dann z�hle die anzahl der Quads hoch
					numExtrudedQuads++;
					//Und f�ge den verarbeiteten Vert zu den im n�chsten loop ausgeschlossenen
					alreadyProcessedVerts.push_back(*vIt);
					//Aber das bedeutet, dass wenn ich einmal im Kreis laufe, ich den hier garnict mehr als sekund�ren Verts mit verwurste?
				}
			}
		}
	}
	//Nun habe ich eine Sammlung aller Vertices mit Beziehungen, welche zu extrudieren sind. 
	//In den Beziehungen steht, welche die alten Verts und die neuen sind und welche benachbart sind
	//Wie f�ge ich jetzt aber dem Graphen die neuen Verts in ccw hinzu?

	//Berechne das Baryzentrm der Quads.
	//Was kann ich nun damit machen? K�nnte ich mich nicht daran orientieren, wie die schon verbundenen Verts orienteirt sind?
	//Ich habe Beziehungen als Edges zwischen zwei Verts in den Quads. Wenn ich heruasfinde, in welcher Richtung diese Verst zusammenh�ngen
	//kann ich den Rest mit dieser Information zusammenf�gen. Daf�r brauche ich kein Baryzentrum.
	//Eine Funktion k�nnte bereits unten stehen, wie ich das boundary Edge herausfinde!

	//mesh.add_face(extrudedQuads[0]);
	Mesh::VertexIter vIt2;
	for (vIt2 = vBegin; vIt2 != vEnd; ++vIt2)
	{
		mesh.status(*vIt2).set_selected(false);
	}

	for (unsigned int i = 0; i < numExtrudedQuads; i++)
	{
		std::cout << "in face add schleife" << std::endl;
		std::cout << "numExtrudedQuads: " << numExtrudedQuads << std::endl;
		Mesh::HalfedgeHandle hh = mesh.find_halfedge(extrudedQuads[i].at(0), extrudedQuads[i].at(2));
		Mesh::HalfedgeHandle hh2 = mesh.find_halfedge(extrudedQuads[i].at(2), extrudedQuads[i].at(0));

		if (hh.is_valid())
		{
			std::cout << "hh is boundary" << std::endl;
			mesh.add_face(extrudedQuads[i].at(0), extrudedQuads[i].at(2), extrudedQuads[i].at(3));
			std::cout << "first Point added" << std::endl;
			mesh.add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(1), extrudedQuads[i].at(0));
			std::cout << "second Point added" << std::endl;
			mesh.status(extrudedQuads[i].at(1)).set_selected(true);
			mesh.status(extrudedQuads[i].at(3)).set_selected(true);
		}

		if (hh2.is_valid())
		{
			std::cout << "hh2 is boundary" << std::endl;
			mesh.add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(0), extrudedQuads[i].at(1));
			std::cout << "first Point added" << std::endl;
			mesh.add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(2), extrudedQuads[i].at(0));
			std::cout << "second Point added" << std::endl;
			mesh.status(extrudedQuads[i].at(1)).set_selected(true);
			mesh.status(extrudedQuads[i].at(3)).set_selected(true);
		}
	}
	translateSelOMStatusBitToColor();


	//Old first try:
	//Mesh::VertexIter vIt, vBegin, vEnd;
	//vBegin = mesh.vertices_begin();
	//vEnd = mesh.vertices_end();

	//Mesh::VertexHandle alreadyProcessedVertex;
	//for (vIt = vBegin; vIt != vEnd; ++vIt)
	//{
	//	if (mesh.status(*vIt).selected() && alreadyProcessedVertex != vIt)
	//	{
	//		for (Mesh::VertexVertexIter vvIter = mesh.vv_iter(*vIt); vvIter; ++vvIter)
	//		{
	//			if (mesh.status(*vvIter).selected())
	//			{
	//				//found second selected vertex
	//				//duplicate the selection
	//				Mesh::VertexHandle vertexC = mesh.add_vertex(mesh.point(*vIt) * 1.1f);
	//				Mesh::VertexHandle vertexD = mesh.add_vertex(mesh.point(*vvIter) * 1.1f);
	//				std::cout << "added points" << std::endl;

	//				Mesh::HalfedgeHandle hh = mesh.find_halfedge(*vIt, *vvIter);
	//				Mesh::HalfedgeHandle hh2 = mesh.find_halfedge(*vvIter, *vIt);

	//				if (mesh.is_boundary(hh))
	//				{
	//					std::cout << "hh is boundary" << std::endl;
	//					mesh.add_face(*vIt, *vvIter, vertexD);
	//					std::cout << "first Point added" << std::endl;
	//					mesh.add_face(vertexD, vertexC, *vIt);
	//					std::cout << "second Point added" << std::endl;
	//				}

	//				if (mesh.is_boundary(hh2))
	//				{
	//					std::cout << "hh2 is boundary" << std::endl;
	//					mesh.add_face(vertexD, *vIt, vertexC);
	//					std::cout << "first Point added" << std::endl;
	//					mesh.add_face(vertexD, *vvIter, *vIt);
	//					std::cout << "second Point added" << std::endl;
	//				}

	//				//add new faces with respect to the right order of the added vertices


	//				//Dont do it again:
	//				alreadyProcessedVertex = vvIter;

	//				//....activate grab mode....
	//				Mesh::VertexIter vIt2;
	//				for (vIt2 = vBegin; vIt2 != vEnd; ++vIt2)
	//				{
	//					mesh.status(*vIt2).set_selected(false);
	//				}

	//				mesh.status(vertexC).set_selected(true);
	//				mesh.status(vertexD).set_selected(true);
	//				translateSelOMStatusBitToColor();
	//				break;
	//			}
	//		}
	//	}
	//}

	//FIXME: I think I need this more than one time ->make an extra function for this and reduce code
	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (int i = 0, nF = mesh.n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = mesh.cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (int j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	mesh.update_normals();
	getNormalArray()->dirty();

}

void OpenMeshGeometry::calcNormals()
{
	//mesh.update_face_normals();
	Mesh::FaceIter  fIt, fBegin, fEnd;
	fBegin = mesh.faces_begin();
	fEnd = mesh.faces_end();
	int i = 0;
	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		Mesh::FaceVertexIter fvIter = mesh.fv_iter(fIt);
		for (; fvIter; ++fvIter)
		{
			mesh.set_normal(fvIter, mesh.calc_face_normal(fIt));
		}
	}

	// move all vertices one unit length along it's normal direction
	//for (Mesh::VertexIter v_it = mesh.vertices_begin();	v_it != mesh.vertices_end(); ++v_it)
	//{
	//	//std::cout << "Vertex #" << v_it << ": " << mesh.point(v_it);
	//	mesh.set_point(v_it, mesh.point(v_it) + mesh.normal(v_it)*0.05);
	//	//std::cout << " moved to " << mesh.point(v_it) << std::endl;
	//}
	getVertexArray()->dirty();

	//mesh.update_normals();
	getNormalArray()->dirty();
}

// set the indices of face to (v1,v2,v3)
void OpenMeshGeometry::sync_indices(size_t face, size_t v1, size_t v2, size_t v3)
{
	OpenMesh::FPropHandleT<GLuint> indicesPropH;
	mesh.get_property_handle(indicesPropH, "indices");
	OpenMesh::PropertyT<GLuint> indices = mesh.property(indicesPropH);

	indices[3 * face] = v1;
	indices[3 * face + 1] = v2;
	indices[3 * face + 2] = v3;
}

//EXP:Stores the original geometry for:
//- restore topology after "escaping" from the tranformation mode
//- used for example in grabVertices() to set an absolut delta-Position between original and transformed mesh
void OpenMeshGeometry::copyAndStoreVertexArray()
{
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getVertexArray());
	osg::MixinVector<osg::Vec3>::iterator it = vertexArray->begin();
	m_initialTopologyAtStartOfTransform->assign(it, it + vertexArray->size());
}

void OpenMeshGeometry::cancelAnyTransformation()
{
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getVertexArray());
	osg::MixinVector<osg::Vec3>::iterator it = m_initialTopologyAtStartOfTransform->begin();
	vertexArray->assign(it, it + m_initialTopologyAtStartOfTransform->size());
	vertexArray->dirty();
}

void OpenMeshGeometry::translateSelColorToOMStatusBit()
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getColorArray());

	int i = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (colorArray->at(i) == m_selectedColor)
		{
			mesh.status(*vIt).set_selected(true);
		}
		else
		{
			mesh.status(*vIt).set_selected(false);
		}
		i++;
	}
}

void OpenMeshGeometry::translateSelOMStatusBitToColor()
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getColorArray());

	int i = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			colorArray->at(i) = m_selectedColor;
		}
		else
		{
			colorArray->at(i) = m_notSelectedColor;
		}
		i++;
	}
	colorArray->dirty();
}

float OpenMeshGeometry::calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh)
{
	float squareDistanceX = pow(mesh.point(vIt.handle()).x() - mesh.point(vh).x(), 2);
	float squareDistanceY = pow(mesh.point(vIt.handle()).y() - mesh.point(vh).y(), 2);
	float squareDistanceZ = pow(mesh.point(vIt.handle()).z() - mesh.point(vh).z(), 2);

	float distance = sqrt(squareDistanceX + squareDistanceY + squareDistanceZ);

	return distance;
}

//TODO: Implement this as historyManager
void OpenMeshGeometry::restoreLastTopology()
{
	int sizeOfArray = m_topologyHistory.size();
	setVertexArray(m_topologyHistory.at(sizeOfArray - 2));

}

void OpenMeshGeometry::removeSeparatedObjects()
{
	//TODO: Not finished work. Is just for fractal2Mesh 
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	std::vector<std::vector<OpenMesh::VertexHandle>*>* selectedVertsBaseVector;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (!mesh.status(*vIt).selected())
		{
			std::vector<OpenMesh::VertexHandle>* selectedVertsInnerVector = new std::vector<OpenMesh::VertexHandle>;
			selectedVertsBaseVector->push_back(selectedVertsInnerVector);
			mesh.status(*vIt).set_selected(true);
			selectedVertsInnerVector->push_back(vIt);
			selectAllConnectedVertices(vIt, selectedVertsInnerVector);
		}
	}

	//Which island is the biggest?
	unsigned int theBiggest = 0;
	unsigned int indexOfBiggest = 0;
	for (int i = 0; i < selectedVertsBaseVector->size(); ++i)
	{
		if (theBiggest < selectedVertsBaseVector->at(i)->size())
		{
			theBiggest = selectedVertsBaseVector->at(i)->size();
			indexOfBiggest = i;
		}
	}

	//delete all other stuff
	for (int i = 0; i < selectedVertsBaseVector->size(); ++i)
	{
		if (indexOfBiggest != i)
		{
			for (int j = 0; j < selectedVertsBaseVector->size(); ++j)
			{
				mesh.status(selectedVertsBaseVector->at(i)->at(j)).set_deleted(true);
			}
		}
	}

	mesh.garbage_collection(true, true, true);


	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (int i = 0, nF = mesh.n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = mesh.cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (int j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	mesh.update_normals();
	getNormalArray()->dirty();

}

void OpenMeshGeometry::selectAllConnectedVertices(OpenMesh::VertexHandle vIt, std::vector<OpenMesh::VertexHandle>* selectedVertsInnerVector)
{
	for (Mesh::VertexVertexIter vvIter = mesh.vv_iter(vIt); vvIter; ++vvIter)
	{
		if (!mesh.status(*vvIter).selected())
		{
			selectedVertsInnerVector->push_back(vvIter);
			mesh.status(*vvIter).set_selected(true);
			selectAllConnectedVertices(vvIter, selectedVertsInnerVector);
		}
	}
}