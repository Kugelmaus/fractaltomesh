//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include <OpenMesh/Core/Mesh/TriMeshT.hh>

namespace OpenMesh {

	// Specialized TriMeshT implementation which synchronizes 
	// external bound index array after each edge collapse and
	// vertex split
	template <class Kernel>
	class OSG_TriMeshT : public TriMeshT < Kernel >
	{
	public:
		typedef typename TriMeshT<Kernel> Base;

		HalfedgeHandle vertex_split(VertexHandle v0, VertexHandle v1, VertexHandle vl, VertexHandle vr)
		{
			HalfedgeHandle result = Base::vertex_split(v0, v1, vl, vr);

			ConstVertexFaceIter cvfiter = cvf_iter(from_vertex_handle(result));
			while (cvfiter)
			{
				FaceHandle fh = cvfiter.handle();

				assert(fh.is_valid());
				HalfedgeHandle hh(halfedge_handle(fh));
				assert(hh.is_valid());
				size_t v1 = to_vertex_handle(hh).idx();
				hh = next_halfedge_handle(hh);
				assert(hh.is_valid());
				size_t v2 = to_vertex_handle(hh).idx();
				hh = next_halfedge_handle(hh);
				assert(hh.is_valid());
				size_t v3 = to_vertex_handle(hh).idx();
				sync_indices(fh.idx(), v1, v2, v3);

				set_normal(fh, calc_face_normal(fh));
				++cvfiter;
			}

			return result;
		}

		void collapse(HalfedgeHandle _heh)
		{
			VertexHandle vh = to_vertex_handle(_heh);

			Base::collapse(_heh);


			ConstVertexFaceIter cvfiter = cvf_iter(vh); //PL: Iterate over all adjacent faces of this vertex
			while (cvfiter)
			{
				FaceHandle fh = cvfiter.handle();

				assert(fh.is_valid());
				HalfedgeHandle hh(halfedge_handle(fh));
				assert(hh.is_valid());
				size_t v1 = to_vertex_handle(hh).idx();
				hh = next_halfedge_handle(hh);
				assert(hh.is_valid());
				size_t v2 = to_vertex_handle(hh).idx();
				hh = next_halfedge_handle(hh);
				assert(hh.is_valid());
				size_t v3 = to_vertex_handle(hh).idx();
				sync_indices(fh.idx(), v1, v2, v3);

				set_normal(fh, calc_face_normal(fh));
				++cvfiter;
			}
		}

		// set the indices of face to (v1,v2,v3)
		void sync_indices(size_t face, size_t v1, size_t v2, size_t v3)
		{
			FPropHandleT<GLuint> indicesPropH;
			get_property_handle(indicesPropH, "indices");
			PropertyT<GLuint> indices = property(indicesPropH);

			indices[3 * face] = v1;
			indices[3 * face + 1] = v2;
			indices[3 * face + 2] = v3;
		}

	};
}