//############################################################################################
// Fractal To Mesh Application - Copyright (C) 2015 Philipp Ladwig
//############################################################################################
#pragma once


#include <OpenMesh/Core/IO/MeshIO.hh>
#include "OpenMeshGeometry.h"


namespace ftm{
	class OutputManager
	{
		// #### CONSTRUCTOR & DESTRUCTOR ###############
	public:
		OutputManager();
		~OutputManager(void);


		// #### MEMBER VARIABLES ###############
	private:

		// #### MEMBER FUNCTIONS ###############
	public:
		//void writeFileToDisk(osg::OpenMeshGeometry* openMeshGeoemtry, std::string safePath);
	};
}//Namespace