@echo off
SET var=PROCESSING FINISHED
SET var2=FINAL STL FILE IN:
@echo on
cd meshlab
CALL meshlabserver.exe -i %1 -o %2 -s %3 -om fn
@echo off
ECHO.
COLOR 02
ECHO ################################################
echo # %var%
ECHO # %var2%
ECHO #
ECHO #   %2
ECHO #
ECHO ################################################
ECHO.
PAUSE